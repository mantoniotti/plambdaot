;;;; -*- Mode: Lisp -*-

;;;; type-declarations.lisp
;;;;
;;;; Plotting utilities for LW; various utility functions and macros.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(in-package "PLAMBDAOT")


(deftype maybe (something)
  "A traditional, Haskell-like, type declaration."
  `(or null ,something))

;;;; end of file -- type-declarations.lisp
