;;;; -*- Mode: Lisp -*-

;;;; dataset-implementation.lisp
;;;
;;;; Plotting utilities for LW; "dataset" implementations.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(in-package "CL-PLOT")


;;;---------------------------------------------------------------------------
;;; Constructors and initializers.

;;; initialize-instance

(defmethod initialize-instance :after ((g dataset-2d) &key)
  (setf (dataset-x-data g)
        (coerce (dataset-x-data g) 'vector))

  (loop for x across (dataset-x-data g)
        when (numberp x) maximize x into xmax
        when (numberp x) minimize x into xmin
        finally (setf (slot-value g 'x-max) xmax
                      (slot-value g 'x-min) xmin))

  (setf (dataset-y-data g)
        (coerce (dataset-y-data g) 'vector))

  (loop for y across (dataset-y-data g)
        when (numberp y) maximize y into ymax
        when (numberp y) minimize y into ymin
        finally (setf (slot-value g 'y-max) ymax
                      (slot-value g 'y-min) ymin))

  )


(defmethod initialize-instance :after ((g dataset-3d) &key)
  (setf (dataset-z-data g)
        (coerce (dataset-z-data g) 'vector))

  (loop for z across (dataset-z-data g)
        when (numberp z) maximize z into zmax
        when (numberp z) minimize z into zmin
        finally (setf (slot-value g 'z-max) zmax
                      (slot-value g 'z-min) zmin)))


;;; Basic protocol.

(defmethod dataset-rank ((d dataset))
  (let ((raw-data (dataset-raw-data d)))
    (etypecase raw-data
      (array (array-rank raw-data))
      (list
       (warn "CL-PLOT: guessing raw data rank for dataset ~S." d)
       1))
    ))

(defmethod dataset-rank ((d dataset-2d))
  2) ; Pairs.

(defmethod dataset-rank ((d dataset-3d))
  2) ; Triples.


(defmethod dataset-dimensions ((d dataset))
  (let ((raw-data (dataset-raw-data d)))
    (etypecase raw-data
      (array (array-dimensions raw-data))
      (list
       (warn "CL-PLOT: guessing raw data dimensions for dataset ~S." d)
       (list (list-length raw-data))))
    ))


(defmethod dataset-dimensions ((d dataset-2d))
  (list (length (dataset-x-data d)) 2))


(defmethod dataset-dimensions ((d dataset-3d))
  (list (length (dataset-x-data d)) 3))
                   

;;; make-dataset*

(defmethod make-dataset* ((data list)
                          &key (name (gensym "DATASET-"))
                          &allow-other-keys) 
  (destructuring-bind (x-data y-data &optional z-data)
      data
    (let ((kind (if z-data :3d :2d)))
      (case kind
        (:2d
         (assert (= (length x-data) (length y-data)))
         (make-instance 'dataset-2d
                        :name name
                        :x-data x-data
                        :y-data y-data))
        (:3d
         (assert (= (* (length x-data) (length y-data)) (length z-data)))
         (make-instance 'dataset-3d
                        :name name
                        :x-data x-data
                        :y-data y-data
                        :z-data z-data)))
      )))


(defmethod make-dataset* ((data vector)
                          &key (name (gensym "DATASET-"))
                          &allow-other-keys)
  (case (length data)
    (1 (make-instance 'dataset-2d
                      :name name
                      :x-data (range 0 (length (aref data 0)))
                      :y-data (aref data 0)))
    (2 (make-instance 'dataset-2d
                      :name name
                      :x-data (aref data 0)
                      :y-data (aref data 1)))
    (3 (make-instance 'dataset-3d
                      :name name
                      :x-data (aref data 0)
                      :y-data (aref data 1)
                      :z-data (aref data 2)))
    (t (make-instance 'dataset-2d
                      :name name
                      :x-data (range 0 (length data))
                      :y-data data))
    ))


(defmethod make-dataset* ((data array)
                          &key (name (gensym "DATASET-"))
                          &allow-other-keys)
  (assert (< 0 (array-rank data) 4))
  (multiple-value-bind (x-data y-data z-data)
      (destructure-dataset data)
    (let ((kind (if z-data :3d :2d)))
      (case kind
        (:2d
         (assert (= (length x-data) (length y-data)))
         (make-instance 'dataset-2d
                        :name name
                        :x-data x-data
                        :y-data y-data))
        (:3d
         (assert (= (length x-data) (length y-data) (length z-data)))
         (make-instance 'dataset-3d
                        :name name
                        :x-data x-data
                        :y-data y-data
                        :z-data z-data)))
      )))


;;; make-dataset

(defun make-dataset (&key 
                     (name (gensym "DATASET-"))
                     (y-data ())
                     (x-data (range 0 (1- (length y-data)) 1))
                     (z-data () z-data-supplied-p))
  (let ((kind (if z-data-supplied-p :3d :2d)))
    (case kind
      (:2d
       (assert (= (length x-data) (length y-data)))
       (make-instance 'dataset-2d
                      :name name
                      :x-data x-data
                      :y-data y-data))
      (:3d
       (assert (= (length x-data) (length y-data) (length z-data)))
       (make-instance 'dataset-3d
                      :name name
                      :x-data x-data
                      :y-data y-data
                      :z-data z-data)))
    ))


;;;---------------------------------------------------------------------------
;;; make-shaped-dataset

;;; dataset methods
;;; These are essentially "pass through"

(defmethod make-shaped-dataset ((data dataset-2d)
                                (shape (eql :line))
                                (dimensions (eql :2d))
                                &key &allow-other-keys)
  data)


;;; :line methods

(defmethod make-shaped-dataset ((data list)
                                (shape (eql :line))
                                (dimensions (eql :2d)) 
                                &key &allow-other-keys)
  (typecase (first data)
    ((or real list)
     ;; Assume this is a list of numbers, i.e. a 1d dataset.
     (multiple-value-bind (xs ys zs)
         (destructure-dataset data)
       (when zs
         (warn "2D line dataset called for, but data is 3D; Zs will be dropped."))
       (make-instance 'dataset-2d :x-data xs :y-data ys)))
    (complex
     (make-instance 'dataset-2d
                    :y-data (mapcar #'imagpart data)
                    :x-data (mapcar #'realpart data)))
    ))


(defmethod make-shaped-dataset ((data array)
                                (shape (eql :line))
                                (dimensions (eql :2d))
                                &key
                                (column-data-p t)
                                &allow-other-keys)
  (ecase (array-rank data)
    (0 (make-instance 'dataset-2d))

    (1 (typecase (array-element-type data) ; Damn defaults!
         (real (make-instance 'dataset-2d
                              :x-data (range 0 (length data))
                              :y-data (copy-seq data)))
         (complex (make-instance 'dataset-2d
                                 :y-data (map 'list #'imagpart data)
                                 :x-data (map 'list #'realpart data)))
         (t
          (typecase (aref data 0)
            (real (make-instance 'dataset-2d
                                 :x-data (range 0 (length data))
                                 :y-data (copy-seq data)))
            (complex (make-instance 'dataset-2d
                                    :y-data (map 'list #'imagpart data)
                                    :x-data (map 'list #'realpart data)))
            (t (error "Cannot plot a vector of non numbers (cfr DATA[0] = ~S)."
                      (aref data 0)))
            ))))

    (2 (if column-data-p
           (make-instance 'dataset-2d
                          :x-data (matrix-column data 0)
                          :y-data (matrix-column data 1))
           (make-instance 'dataset-2d
                          :x-data (matrix-row data 0)
                          :y-data (matrix-row data 1))
           ))
    ))


;;; :bar methods

(defun make-group-dataset (dataset-class data x-data)
  (make-instance dataset-class :raw-data data :x-data x-data))


(defmethod make-shaped-dataset ((data list)
                                (shape (eql :bar))
                                (dimensions (eql :2d))
                                &key &allow-other-keys)
  ;; Assumes the list contains sublists of the following forms, no
  ;; error checking done:
  ;;
  ;;	(n1i n2i n3i ... nki)
  ;; or
  ;;	(ki (n1i n2i n3i ... nki))
  ;;
  ;; In the first case the sublist is considered to be a "group"
  ;; indexed by position.  In the second case it is considered to be a
  ;; "group" indexed by 'ki'.
  
  (let ((plain-group-p (not (listp (second (first data))))))
    (if plain-group-p
        (make-group-dataset 'bar-dataset
                            data
                            (range 0 (list-length data)))
        (make-group-dataset 'bar-dataset
                            (mapcar #'second data)
                            (mapcar #'first data))
        )))


(defmethod make-shaped-dataset ((data array)
                                (shape (eql :bar))
                                (dimensions (eql :2d)) 
                                &key
                                (header-row-p nil)
                                (x-data
                                 (data-index-vector data))
                                &allow-other-keys)

  (declare (ignore header-row-p))

  ;; Assumes the array is a 2D matrix.
  
  (assert (= (length x-data) (array-dimension data 0)))
  (make-group-dataset 'bar-dataset data x-data))


;;; :area methods

(defmethod make-shaped-dataset ((data list)
                                (shape (eql :area))
                                (dimensions (eql :2d))
                                &key &allow-other-keys)
  ;; Assumes the list contains sublists of the following forms, no
  ;; error checking done:
  ;;
  ;;	(n1i n2i n3i ... nki)
  ;; or
  ;;	(ki (n1i n2i n3i ... nki))
  ;;
  ;; In the first case the sublist is considered to be a "group"
  ;; indexed by position.  In the second case it is considered to be a
  ;; "group" indexed by 'ki'.
  
  (let ((plain-group-p (not (listp (second (first data))))))
    (if plain-group-p
        (make-group-dataset 'area-dataset data (range 0 (list-length data)))
        (make-group-dataset 'area-dataset  (mapcar #'second data) (mapcar #'first data))
        )))


(defmethod make-shaped-dataset ((data array)
                                (shape (eql :area))
                                (dimensions (eql :2d))
                                &key
                                (header-row-p nil)
                                (x-data
                                 (data-index-vector data))
                                &allow-other-keys)

  (declare (ignore header-row-p))

  ;; Assumes the array is a 2D matrix.
  
  (assert (= (length x-data) (array-dimension data 0)))
  (make-group-dataset 'area-dataset data x-data))


;;; :stack methods

(defmethod make-shaped-dataset ((data list)
                                (shape (eql :stack))
                                (dimensions (eql :2d))
                                &key &allow-other-keys)
  ;; Assumes the list contains sublists of the following forms, no
  ;; error checking done:
  ;;
  ;;	(n1i n2i n3i ... nki)
  ;; or
  ;;	(ki (n1i n2i n3i ... nki))
  ;;
  ;; In the first case the sublist is considered to be a "group"
  ;; indexed by position.  In the second case it is considered to be a
  ;; "group" indexed by 'ki'.
  
  (let ((plain-group-p (not (listp (second (first data))))))
    (if plain-group-p
        (make-group-dataset 'stack-dataset data (range 0 (list-length data)))
        (make-group-dataset 'stack-dataset  (mapcar #'second data) (mapcar #'first data))
        )))


(defmethod make-shaped-dataset ((data array)
                                (shape (eql :stack))
                                (dimensions (eql :2d))
                                &key
                                (header-row-p nil)
                                (x-data
                                 (data-index-vector data))
                                &allow-other-keys)

  (declare (ignore header-row-p))

  ;; Assumes the array is a 2D matrix.
  
  (assert (= (length x-data) (array-dimension data 0)))
  (make-group-dataset 'stack-dataset data x-data))


;;;---------------------------------------------------------------------------
;;; Destructuring various datasets.
;;;
;;; TODO: add new formats. Vectors of points etc etc.

(defun dotted-pair-p (x)
  (and (consp x) (cdr x) (atom (cdr x))))


(defun pairp (x)
  (or (dotted-pair-p x)
      (and (vectorp x) (= 2 (length x)))
      (and (listp x) (null (cdr (last x))) (= (list-length x) 2))))


(defun triplep (x)
  (and (typep x 'sequence) (= 3 (length x))))


(defun pair-0 (x)
  (assert (pairp x))
  (cond ((or (dotted-pair-p x)
             (and (listp x) (null (cdr (last x))) (= (list-length x) 2)))
         (car x))
        ((and (vectorp x) (= 2 (length x))) (aref x 0))))


(defun pair-1 (x)
  (assert (pairp x))
  (cond ((dotted-pair-p x) (cdr x))
        ((and (listp x) (null (cdr (last x))) (= (list-length x) 2))
         (second x))
        ((and (vectorp x) (= 2 (length x))) (aref x 1))))


(defmethod destructure-dataset ((a array))
  (case (array-rank a)
    (0 ())
    (1 (copy-seq a))
    (2 (values (loop for i from 0 below (array-dimension a 0)
                     collect (aref a i 0))
               (loop for i from 0 below (array-dimension a 1)
                     collect (aref a i 1))))
    (3 (values (loop for i from 0 below (array-dimension a 0)
                     collect (aref a i 0))
               (loop for i from 0 below (array-dimension a 1)
                     collect (aref a i 1))
               (loop for i from 0 below (array-dimension a 2)
                     collect (aref a i 2))))
    ))


(defmethod destructure-dataset ((a list))
  (cond ((every #'numberp a) 
         (values (loop for nil in a ; Check out the NIL! Cute!
                       for x from 0
                       collect x)
                 (copy-list a)))
        ((every #'pairp a)
         (values (loop for x in a
                       collect (pair-0 x))
                 (loop for x in a
                       collect (pair-1 x))))
        ((every #'triplep a)
         (values (loop for x in a collect (elt x 0))
                 (loop for x in a collect (elt x 1))
                 (loop for x in a collect (elt x 2))
                 ))
        (t
         (error "Badly shaped list dataset; only singletons, pairs and triplets accepted."))
        ))

;;;---------------------------------------------------------------------------
;;; Extremes.

;;; These are catch-all methods and are incorrect.

(defmethod dataset-x-max ((d dataset)) 0)
(defmethod dataset-y-max ((d dataset)) 0)
(defmethod dataset-z-max ((d dataset)) 0)

(defmethod dataset-x-min ((d dataset)) 0)
(defmethod dataset-y-min ((d dataset)) 0)
(defmethod dataset-z-min ((d dataset)) 0)

(defmethod dataset-z-max ((d dataset-2d)) 0)
(defmethod dataset-z-min ((d dataset-2d)) 0)


;;;---------------------------------------------------------------------------
;;; Computing the normalized points.
;;;
;;; The "normalized points" are translated and scaled to the interval [-1, 1].
;;; Essentially a scale and translation without the graphics
;;; machinery. Think of the MODELVIEW matrix in OpenGL.
;;;

#|
The transformations we use are described hereafter
We want the y on [-1, 1] corresponding to x.

1.
ymin = m xmin + d
ymax = m xmax + d

Given: y = m x + d

2.
d = ymin - m xmin
ymax = m xmax + ymin - m xmin

3.
d = ymin - m xmin
ymax = m (xmax - xmin) + ymin

4.
d = ymin - m xmin
ymax - ymin = m (xmax - xmin)

5.
d = ymin - m xmin
m = (ymax - ymin) / (xmax - xmin)

6.
d = ymin - (((ymax - ymin) / (xmax - xmin)) xmin)
m = (ymax - ymin) / (xmax - xmin)

7.
d = (1/(xmax - xmin)) (ymin (xmax - xmin) - xmin (ymax - ymin))
m = (ymax - ymin) / (xmax - xmin)

8.
d = (1/(xmax - xmin)) (ymin xmax - ymin xmin - xmin ymax + xmin ymim)
m = (ymax - ymin) / (xmax - xmin)

9.
d = (1/(xmax - xmin)) (ymin xmax - xmin ymax)
m = (ymax - ymin) / (xmax - xmin)

In the following ymin = -1 and ymax = 1.
|#

;;; Notes:
;;;
;;; 20230118 MA:
;;; The code below is "plain", but the actual
;;; normalization should be parametric, in the sense that it should
;;; consider the actual "bounds" of the interval being handled.
;;; In the simplest, but not trivial, case, we have a plot from "min"
;;; to "max" that must be "adapted" in order to have "ticks" in "nice"
;;; positions.
;;; The trivial case is for points that must just be used "as-is",
;;; without any attempt to "adapt" the ticks intervals.
;;; After this comment is committed and a tag produced, I will begin
;;; modifying the code.

(defparameter *enclose-bounds* t)


(defgeneric compute-normalized-points (dataset
                                       &key
                                       extend-range
                                       result-type
                                       &allow-other-keys)
  (:documentation
   "Projects the points in DATASET onto the interval [-1, 1].

The result is one or more sequences of type RESULT-TYPE.  If
EXTEND-RANGE is true, the dataset, or the axes of the dataset, may be
extended by one extra point below the minimum and one extra point
above the maximum.")
  )


(defmethod dataset-normalized-points :before ((d dataset))
  (unless (and (slot-boundp d 'normalized-points)
               (slot-value d 'normalized-points))
    (compute-normalized-points d :extend-range *enclose-bounds*)))


(defmethod compute-normalized-points ((d dataset)
                                      &key
                                      extend-range
                                      result-type
                                      &allow-other-keys)
  (declare (ignore extend-range result-type))
  ;; No-op.
  )


(defun normalize-pt-unit-interval (pt min max)
  "Maps a number PT in the unit interval.

The number (or point) PT must taken form the interval [MIN, MAX]."

  (declare (type real pt min max))

  (assert (and (< min max)
               (<= min pt max))     
      (pt min max))

  (let* ((range (- max min))
         (mx (/ 2.0 range)) ; -2.0 = (- (- 1 -1)); What was I thinking?
         (dx (* (/ range)
                ;; (- (* -1.0 max) (* min 1.0))
                (- (- max) min)
                ))
         )
    (declare (type real range mx dx))
    (+ (* mx pt) dx)
    ))

(defun normalize-d-unit-interval (d max)
  "Maps a distance D with respect to the unit interval [-1, 1].

The number (or point) PT must taken form the interval [0, MAX]."

  (declare (type real d max))

  (assert (<= 0.0 d max) (d max))

  (let* ((range max)
         (mx (/ 2.0 range)) ; -2.0 = (- (- 1 -1)); What was I thinking?
         
         #|
         (dx (* (/ range)
                ;; (- (* 0.0 max) (* 0.0 2.0))
                0
                ))
         |#
         )
    (declare (type real range mx))
    ;; (+ (* mx d) dx)
    (* mx d)
    ))


(defgeneric normalize-data-to-unit-interval (data &key sort test dimensions)
  )


(defmethod normalize-data-to-unit-interval ((data sequence)
                                            &key sort
                                            (test #'<)
                                            (dimensions 1))
  (let ((seq (if sort (sort (copy-seq data) test) data)))
    (case dimensions
      (1 (normalize-points-1d seq))
      (2 (normalize-points-2d (map 'list #'(lambda (pt) (elt pt 0)) data)
                              (map 'list #'(lambda (pt) (elt pt 1)) data)))
      ))
  )


(defun normalize-points-1d (ptsx)
  ;; PTSX is an ordered vector.
  (declare (type (or list vector) ptsx))

  (assert (> (length ptsx) 1) (ptsx))

  (let* ((dxmax (elt ptsx (1- (length ptsx))))
         (dxmin (elt ptsx 0))
         (xwidth (- dxmax dxmin))

         (mx (- (/ -2.0 xwidth))
             ;; (- (/ (- -1.0 1.0) (- dxmax dxmin)))
             )
         (dx (- (/ (- dxmin (- dxmax)) xwidth))
             ;; (- (/ (- (* dxmin 1.0) (* dxmax -1.0)) (- dxmax dxmin)))
             )
         )

    (declare (type real dxmax dxmin xwidth mx dx))

    (flet ((norm-coord (x m d)
             (declare (type real x m d))
             (+ (* m x) d))
           )
      (declare (ftype (function (real real real) real) norm-coord))

      (map 'list #'(lambda (x) (declare (type real x)) (norm-coord x mx dx)) ptsx)
      )))


(defun normalize-points-2d (ptsx ptsy)
  ;; PTSX and PTSY are ordered vectors.
  (declare (type vector ptsx ptsy))
  (let* ((dxmax (aref ptsx (1- (length ptsx))))
         (dxmin (aref ptsx 0))
         (dymax (aref ptsy (1- (length ptsx))))
         (dymin (aref ptsy 0))
             
         (xwidth (- dxmax dxmin))
         (ywidth (- dymax dymin))
            
         (mx (- (/ -2.0 xwidth))
             ;; (- (/ (- -1.0 1.0) (- dxmax dxmin)))
             )
         (dx (- (/ (- dxmin (- dxmax)) xwidth))
             ;; (- (/ (- (* dxmin 1.0) (* dxmax -1.0)) (- dxmax dxmin)))
             )
       
         (my (- (/ -2.0 ywidth))
             ;; (- (/ (- -1.0 1.0) (- dymax dymin)))
             )
         (dy (- (/ (- dymin (- dymax)) ywidth))
             ;; (- (/ (- (* dymin 1.0) (* dymax -1.0)) (- dymax dymin)))
             )
         )

    (declare (type real
                   dxmax dxmin
                   dymax dymin
                   xwidth ywidth
                   mx dx my dy))

    (flet ((norm-coord (x m d)
             (declare (type real x m d))
             (+ (* m x) d))
           )
      (declare (ftype (function (real real real) real) norm-coord))

      (loop for x across ptsx
            for y across ptsy
            if (numberp y)
              collect (list (norm-coord x mx dx) (norm-coord y my dy))
            else
              collect (list (norm-coord x mx dx) :missing)
            end
            )
      )))


(defmethod compute-normalized-points ((ds dataset-2d)
                                      &key
                                      extend-range
                                      (result-type 'list)
                                      &allow-other-keys)
  (declare (type boolean extend-range)
           (ignore result-type))

  (let ((dxmax (dataset-x-max ds))
        (dxmin (dataset-x-min ds))
        (dymax (dataset-y-max ds))
        (dymin (dataset-y-min ds))
        )

    (declare (type real dxmax dxmin dymax dymin))

    (when extend-range
      ;; multiple-value-select is defined in 'utilities.lisp'.
      (multiple-value-setq (dxmin dxmax)
          (multiple-value-select (enclosing-bounds dxmin dxmax) 0 2))

      (multiple-value-setq (dymin dymax)
          (multiple-value-select (enclosing-bounds dymin dymax) 0 2))
      )
             
    (let* ((xwidth (- dxmax dxmin))
           (ywidth (- dymax dymin))
            
           (mx (- (/ -2.0 xwidth))
               ;; (- (/ (- -1.0 1.0) (- dxmax dxmin)))
               )
           (dx (- (/ (- dxmin (- dxmax)) xwidth))
               ;; (- (/ (- (* dxmin 1.0) (* dxmax -1.0)) (- dxmax dxmin)))
               )
       
           (my (- (/ -2.0 ywidth))
               ;; (- (/ (- -1.0 1.0) (- dymax dymin)))
               )
           (dy (- (/ (- dymin (- dymax)) ywidth))
               ;; (- (/ (- (* dymin 1.0) (* dymax -1.0)) (- dymax dymin)))
               )
           )

      (declare (type real xwidth ywidth mx dx my dy))

      (flet ((norm-coord (x m d)
               (declare (type real x m d))
               (+ (* m x) d))
             )
        (declare (ftype (function (real real real) real) norm-coord))

        (setf (dataset-normalized-points ds)
              (loop for x across (dataset-x-data ds)
                    for y across (dataset-y-data ds)
                    if (numberp y)
                      collect (list (norm-coord x mx dx) (norm-coord y my dy))
                    else
                      collect (list (norm-coord x mx dx) :missing)
                    end
                    )
              ))))
  )


(defmethod compute-normalized-points ((ds dataset-3d)
                                      &key
                                      extend-range
                                      (result-type 'list)
                                      &allow-other-keys)
  ;; Essentially a scale and translation without the graphics
  ;; machinery.  Think of the MODELVIEW matrix in OpenGL.

  (declare (type boolean extend-range)
           (ignore result-type))

  (let ((dxmax (dataset-x-max ds))
        (dxmin (dataset-x-min ds))

        (dymax (dataset-y-max ds))
        (dymin (dataset-y-min ds))

        (dzmax (dataset-z-max ds))
        (dzmin (dataset-z-min ds))
        )

    (declare (type real dxmax dxmin dymax dymin dzmax dzmin))

    (when extend-range
      ;; multiple-value-select is defined in 'utilities.lisp'.
      (multiple-value-setq (dxmin dxmax)
          (multiple-value-select (enclosing-bounds dxmin dxmax) 0 2))

      (multiple-value-setq (dymin dymax)
          (multiple-value-select (enclosing-bounds dymin dymax) 0 2))

      (multiple-value-setq (dzmin dzmax)
          (multiple-value-select (enclosing-bounds dzmin dzmax) 0 2))
      )

    (let* ((xwidth (- dxmax dxmin))
           (ywidth (- dymax dymin))
           (zwidth (- dzmax dzmin))
            
           (mx (- (/ -2.0 xwidth))
               ;; (- (/ (- -1.0 1.0) (- dxmax dxmin)))
               )
           (dx (- (/ (- dxmin (- dxmax)) xwidth))
               ;; (- (/ (- (* dxmin 1.0) (* dxmax -1.0)) (- dxmax dxmin)))
               )
       
           (my (- (/ -2.0 ywidth))
               ;; (- (/ (- -1.0 1.0) (- dymax dymin)))
               )
           (dy (- (/ (- dymin (- dymax)) ywidth))
               ;; (- (/ (- (* dymin 1.0) (* dymax -1.0)) (- dymax dymin)))
               )

           (mz (- (/ -2.0 zwidth))
               ;; (- (/ (- -1.0 1.0) (- dzmax dzmin)))
               )
           (dz (- (/ (- dzmin (- dzmax)) zwidth))
               ;; (- (/ (- (* dzmin 1.0) (* dzmax -1.0)) (- dzmax dzmin)))
               )
           )

      (declare (type real xwidth ywidth zwidth mx dx my dy mz dz))

      (flet ((norm-coord (x m d)
               (declare (type real m d)
                        (type (or real symbol) x))
               (if (numberp x) (+ (* m (the real x)) d) :missing))
             )
        (declare (ftype (function ((or symbol real) real real)
                                  (or (eql :missing) real))
                        norm-coord))

        (setf (dataset-normalized-points ds)
              (loop for x across (dataset-x-data ds)
                    for y across (dataset-y-data ds)
                    for z across (dataset-z-data ds)
                    collect (list (norm-coord x mx dx)
                                  (norm-coord y my dy)
                                  (norm-coord z mz dz))
                      ))
        )))
  )

  
;;;---------------------------------------------------------------------------
;;; Utilities.

(defun extreme (seq &key (test #'<) (start 0) end)
  (declare (type sequence seq)
           (ftype (function (t t) boolean) test)
           (type fixnum start)
           (type (or null fixnum) end))
                 
  (loop with lseq of-type fixnum = (length seq)
        with extr = (elt seq start)
        with extr-pos of-type fixnum = start
        with end of-type fixnum = (if end (min end lseq) lseq)
        for i of-type fixnum from start below end
        for e = (elt seq i)
        when (funcall test e extr)
          do (setf extr e extr-pos i)
        finally (return (values extr extr-pos))))

#+nil
(defmacro do-points (graph &body forms)
  `(loop for x in (dataset-x-data ,graph)
         for y in (dataset-y-data ,graph)
         do (progn ,@forms)
         ))

(declaim (inline map-points))

(defun map-points (result-type f graph)
  (map result-type f (graph-datasets graph)))


(defun data-index-vector (data)
  (declare (type array data))
  (make-array (array-dimension data 0)
              :element-type (array-element-type data)
              :initial-contents (range 0 (array-dimension data 0))))


(defun matrix-column (matrix j)
  (loop for i from 0 below (array-dimension matrix 0)
        collect (aref matrix i j)))


(defun matrix-row (matrix j)
  (loop for i from 0 below (array-dimension matrix 1)
        collect (aref matrix j i)))


;;; Other predicates.

(defun every-dataset-number-value (seq)
  (declare (type sequence seq))
  (every #'(lambda (x) (typep x 'dataset-number-value)) seq))


(defun some-missing-value (seq)
  (declare (type sequence seq))
  (some #'(lambda (x) (typep x 'missing)) seq))


;;; Range inspection functions.
;;;
;;; Notes:
;;;
;;; 20230113 MA: all functions hereafter rely on POSITION* and
;;; friends; they are as good as the undelying implementation.  After
;;; profiling, some of them could be made faster by doing binary
;;; searches, but the result may turn out to be even hairier because
;;; of the :MISSING values.

(defun sorted-seq-p (seq &key (test #'<) (key #'identity))
  (declare (type sequence seq))
  (let ((l (length seq)))
    (declare (type fixnum seq))
    (if (or (zerop l) (= 1 l))
        t
        (loop for i of-type fixnum from 0 below (1- l)
              for x = (elt seq i)
              for y = (elt seq (1+ i))
              always (or (is-missing x)
                         (is-missing y)
                         (funcall test
                                  (funcall key x)
                                  (funcall key y))))
        )))


#+you-are-an-idiot
(defun seq-min (seq)
  (declare (type sequence seq))

  (if (every-dataset-number-value seq)
      (let* ((first-number-pos (position-if #'numberp seq))
             (first-number (when first-number-pos
                             (elt seq first-number-pos)))
             (second-number-pos (position-if #'numberp seq
                                             :start (1+ first-number-pos)))
             (second-number (when second-number-pos
                              (elt seq second-number-pos)))
             )
        (declare (type (or null fixnum)
                       first-number-pos
                       second-number-pos))

        ;; Just a heuristics to get the first "number" or its
        ;; guessed/made-up value.

        (cond ((and first-number-pos (zerop first-number-pos))
               (values first-number 0))

              ;; All :missing values.
              ((null first-number-pos)
               (values :missing 0))

              ;; The first number is not in pos 0.
              (second-number-pos
               (let* ((span (- second-number first-number))
                      (zero-pos-fictional-value
                       (- first-number (* first-number-pos span)))
                      )
                 ;; This is just a heuristics to assign a "value" to
                 ;; the missing item in the "first position".
                 (values zero-pos-fictional-value
                         first-number-pos
                         span)))
              ;; Just one non missing value.
              (t
               (values first-number first-number-pos))
              ))
      (values (elt seq 0) 0)))


(defun compare-missing (test x y)
  (cond ((and (numberp x) (numberp y)) (funcall test x y))
        ((or (is-missing x) (is-missing y)) nil)
        (t (error "CL-PLOT: cannot compare values ~S and ~S." x y))
        ))


(defun seq-min (seq)
  (let ((m (extreme seq :test #'(lambda (x y) (compare-missing #'< x y)))))
    (values m
            (position m seq :test #'equal))))
            


#+you-are-an-idiot
(defun seq-max (seq &aux (l1 (1- (length seq))))
  (declare (type sequence seq)
           (type fixnum l1))

  (if (every-dataset-number-value seq)
      (let* ((last-number-pos
              (position-if #'numberp seq :from-end t))

             (last-number
              (when last-number-pos
                (elt seq last-number-pos)))

             (second-to-last-number-pos
              (position-if #'numberp seq
                           :end last-number-pos
                           :from-end t))
             
             (second-to-last-number
              (when second-to-last-number-pos
                (elt seq second-to-last-number-pos)))
             )
        (declare (type (or null fixnum)
                       last-number-pos
                       second-to-number-number-pos))

        ;; Just a heuristics to get the last "number" or its
        ;; guessed/made-up value.

        (cond ((and last-number-pos (= last-number-pos l1))
               (values last-number l1))

              ;; All :missing values.
              ((null last-number-pos)
               (values :missing l1))

              ;; The last number is not in pos l1.
              (second-to-last-number-pos
               (let* ((span (- last-number second-to-last-number))
                      (zero-pos-fictional-value
                       (+ last-number (* (- l1 last-number-pos) span)))
                      )
                 ;; This is just a heuristics to assign a "value" to
                 ;; the missing item in the "last position".
                 (values zero-pos-fictional-value
                         last-number-pos
                         span)))
              ;; Just one non missing value.
              (t
               (values last-number last-number-pos))
              ))
      (values (elt seq l1) l1)))


(defun seq-max (seq)
  (let ((m (extreme seq :test #'(lambda (x y) (compare-missing #'> x y)))))
    (values m
            (position m seq :test #'equal))))


(defun zero-pos (seq)
  (declare (type sequence seq))
  (position-if #'(lambda (x) (and (numberp x) (zerop x))) seq))


(defun zero-bracket (seq)
  (declare (type sequence seq))
  (let ((zpos (zero-pos seq)))
    (if zpos
        (values zpos zpos)
        (let ((first-neg-pos
               (position-if #'(lambda (x)
                                (and (numberp x) (minusp x)))
                            seq
                            :from-end t))
              (first-pos-pos
               (position-if #'(lambda (x)
                                (and (numberp x) (plusp x)))
                            seq))
              )
          (values first-neg-pos first-pos-pos)))))


(defun y-zero-intercept (x1 y1 x2 y2)
  (declare (type real x1 y1 x2 y2))
  (/ (- (* x2 y1) (* x1 y2)) (- x2 x1)))

(declaim (ftype (function (real real real real) real)
                y-zero-intercept)
         (inline y-zero-intercept))


(defun impute-zero-point (seq)
  (declare (type sequence seq))
  (multiple-value-bind (n p)
      (zero-bracket seq)

    (declare (type (or null fixnum) n p))

    (cond ((or (null n) (null p)) nil)
          ((= n p) (elt seq n))
          (t (let ((pn (elt seq n))
                   (pp (elt seq p))
                   )
               (locally
                 (declare (type real pn pp)
                          (type fixnum pn pp n p))
                 (y-zero-intercept (- n) pn (- p n) pp))
               ))))
  )
          

(defun contains-zero-p (seq)
  ;; Could be faster...
  (or (not (null (zero-pos seq)))
      (and (find-if #'(lambda (x) (and (numberp x) (minusp x))) seq)
           (find-if #'(lambda (x) (and (numberp x) (plusp x))) seq)
           t)))


;;; The following is code to come up with a sensible "axis landmark
;;; points", in the spirit of what Mathematica does.

;;; Essentially we try to choose between an "even" (6 subintervals, 8
;;; landmarks) or an "odd" (5 subintervals, 7 landmarks) breakdown of
;;; the axis.  FTTB, just look at what Mathematica does.

(defconstant +landmark-5+ 5) ; Should work also with fascist SBCL.

(defconstant +landmark-6+ 6) ; Ditto.


;;; The problem: I have a "span" of values to draw where
;;; span = (max - min).
;;; 'min' and 'max' may have different "magnitudes" (as log_10); e.g.,
;;; min = -0.04 and max = 10033; in this case the magnitude of min is
;;; -2 and that of max is 4.
;;; Suppose I wanted to split in 4, or 5 major "subspans".  I want to
;;; have, as a result, something like
;;;
;;; (-2000 0 2000 4000 6000 8000 10000 12000)
;;;
;;; Which I can clip later on.

(defun seq-span-landmarks (seq &aux (l (length seq)))
  (declare (type sequence seq)
           (type fixnum l)
           (ignore l))

  (if (every-dataset-number-value seq)
      (let ((min-value (seq-min seq))
            (max-value (seq-max seq))
            )
        (cond ((and (numberp min-value) (numberp max-value))
               (let* ((span (- max-value min-value))
                      (r5 (nth-value 1 (fround span +landmark-5+)))
                      (r6 (nth-value 1 (fround span +landmark-6+)))
                      (five-better-than-six
                       (< (abs r5) (abs r6)))
                      (subspan
                       (if five-better-than-six
                           ;; Five landmarks will waste less space.
                           (/ span +landmark-5+)
                           (/ span +landmark-6+)))
                      (subspans
                       (if five-better-than-six
                           +landmark-5+
                           +landmark-6+))
                      (neg-landmarks ())
                      (pos-landmarks ())
                      )
                 (declare (ignore subspans)
                          (type list neg-landmarks pos-landmarks)
                          (type real span r5 r6 subspan subspans)
                          (type boolean five-better-than-six)
                          )
                 (cond ((or (and (minusp min-value)
                                 (minusp max-value))
                            (and (plusp min-value)
                                 (plusp max-value)))
                        (loop for landmark = min-value
                                then (+ landmark subspan)
                              collect landmark
                              while (< landmark max-value)))

                       ((and (minusp min-value)
                             (plusp max-value))
                        (loop for landmark = 0.0
                                then (+ landmark subspan)
                              collect landmark into pos-lm
                              while (< landmark max-value)
                              finally
                                (setq pos-landmarks pos-lm))
                        (loop for landmark = (- subspan)
                                then (- landmark subspan)
                              collect landmark into neg-lm
                              while (> landmark min-value)
                              finally
                                (setq neg-landmarks (reverse neg-lm)))
                        (append neg-landmarks pos-landmarks)
                        ))
                 ))
              (t (copy-seq seq)))
        )

      ;; ... else, if we have a seq with symbolic/categorical values,
      ;; just copy it as is.
      (copy-seq seq)
      ))


(defun seq-mag-landmarks (seq &aux (l (length seq)))
  (declare (type sequence seq)
           (type fixnum l)
           (ignore l))
  
  (if (every-dataset-number-value seq)
      (let* ((min-value (seq-min seq))
             (max-value (seq-max seq))
             (min-mag (magnitude min-value))
             (max-mag (magnitude max-value))
             (mag (max min-mag max-mag))
             (min-mag-value (* (signum min-value) (expt 10 min-mag)))
             (max-mag-value (* (signum max-value) (expt 10 mag)))
             (span (- max-value min-value))
             (subspan (/ span 6))
             (subspan-norm (subspan-norm subspan))
             )
        (values mag min-mag-value max-mag-value span subspan subspan-norm)
        )
      (copy-seq seq)))


(defun span-norm (span &optional (n-subspans 5) (mag (magnitude span)))
  (round (* (round (* (/ span n-subspans) (expt 10 (- (1- mag)))))
            (expt 10 (1- mag)))))


(defun subspan-norm (subspan &optional (mag (magnitude subspan)))
  (round (* (round (* subspan (expt 10 (- mag))))
            (expt 10 mag))))


(defun seq-min-max-bounds (seq)
  "Returns the 'bounds' of a sequence based on the concept of 'step'.

The 'step' is a number derived from the minimum and maximum value of
sequence SEQ (which must be ordered).  In general it is 10^M, with M
being one less than the magnitude of the span between the minimum and
maximum of the sequence.  Once the step is known, the lower bound will
be on the multiple of the step closest to but less than the minimum
and the upper bound will be on the multiple of the step closes to,
but greater than the maximum.

The sequence SEQ can contain :missing values but bust be in increaing
order.

Syntax:

seq-min-max-bounds SEQ => LOW, LOW-OFFSET, HIGH, HIGH-OFFSET, STEP

Arguments and Values:

SEQ -- a SEQUENCE
LOW -- a REAL
LOW-OFFSET -- a REAL
HIGH -- a REAL
HIGH-OFFSET -- a REAL
STEP -- a REAL
STEP-MAG -- a FIXNUM
"
  (declare (type sequence seq))
  (enclosing-bounds (seq-min seq) (seq-max seq)))


(defun enclosing-bounds (start end)
  "Returns the 'bounds' of a sequence based on the concept of 'step'.

The 'step' is a number derived from the START and END values.  In
general it is 10^M, with M being one less than the magnitude of the
span between START and END.  Once the step is known, the lower bound
will be on the multiple of the step closest to but less than the
minimum and the upper bound will be on the multiple of the step
closest to, but greater than the maximum.

Syntax:

enclosing-bounds START END => LOW, LOW-OFFSET, HIGH, HIGH-OFFSET, STEP

Arguments and Values:

START -- a REAL
END -- a REAL
LOW -- a REAL
LOW-OFFSET -- a REAL
HIGH -- a REAL
HIGH-OFFSET -- a REAL
STEP -- a REAL
STEP-MAG -- a FIXNUM

Notes:

Even if START = END the relation LOW < HIGH will be true of the
results.
"
  (declare (type real start end))

  (assert (<= start end) (start end))

  (multiple-value-bind (step step-mag)
      (choose-step start end)
    (declare (type real step)
             (type fixnum step-mag))
    (multiple-value-bind (low left-offset)
        (floor start step)
      (multiple-value-bind (high right-offset)
          (ceiling end step)
        (values (* low step)
                left-offset
                (* high step)
                right-offset
                step
                step-mag
                )
        ))))

;;; end of file -- dataset-implementation.lisp --
