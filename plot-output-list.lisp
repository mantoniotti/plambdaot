;;; -*- Mode: Lisp -*-

(in-package "CL-PLOT")

;;; THIS BECOMES A PROPERTY OF THE PLOTS.



;;;---------------------------------------------------------------------------
;;; Plot outputs list

(defvar *plot-outputs* ())

(defvar *current-plot-output* nil)


(defmethod add-plot-output ((po plot-output))
  (push po *plot-outputs*)
  (setf *current-plot-output* po))


(defmethod remove-plot-output ((po plot-output))
  (setf *plot-outputs* (delete po *plot-outputs*))
  (when (eq po *current-plot-output*)
    (setf *current-plot-output* nil))
  *plot-outputs*)


(defmethod select-plot-output ((x fixnum))
  (cond ((> x (length *plot-outputs*))
         (warn "There are less that ~D plot outputs registered.~@
                The current one (~S) will be returned."
               x
               (current-plot-output))
         (current-plot-output))
        (t
         (setf *current-plot-output*
               (elt *current-plot-outputs* x)))
        ))


(defun current-plot-output ()
  *current-plot-output*)

(defun list-plot-outputs ()
  (copy-list *plot-outputs*))

;;; end of file -- plot-output-list.lisp --
