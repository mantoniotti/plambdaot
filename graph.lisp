;;;; -*- Mode: Lisp -*-
;;;;
;;;; graph.lisp --
;;;; Cleaned up version of CAPI plot protocol implementation.
;;;;
;;;; See the file COPYING in top folder for copyright and licensing information.


(in-package "CL-PLOT")

(defclass graph ()
  ((name :accessor graph-name
         :initarg :name)

   (datasets :accessor graph-datasets
             :initform ())

   (in-frame :accessor in-frame ; Support for plot component protocol.
             :initarg :in-frame

             ;; I could do this, but let's not push it.  It reads
             ;; better at the bottom of the file.
             ;;
             ;; :reader part-of
             ;; :reader parent
             )
   
   )
  (:default-initargs
   :name (gensym "GRAPH-")
   :in-frame nil
   ))


(defgeneric graph-p (x)
  (:method ((x graph)) t)
  (:method ((x t)) nil))


(defclass cartesian-graph (graph) ())

(defgeneric cartesian-graph-p (x)
  (:method ((x cartesian-graph)) t)
  (:method ((x t)) nil))


(defclass graph-2d (cartesian-graph)
  ((x-labels :accessor x-labels :initarg :x-labels)
   (y-labels :accessor y-labels :initarg :y-labels)
   )
  (:default-initargs :x-labels () :y-labels ()))

(defgeneric graph-2d-p (x)
  (:method ((x graph-2d)) t)
  (:method ((x t)) nil))


(defclass graph-3d (graph-2d)
  ((z-labels :accessor z-labels :initarg :z-labels))
  (:default-initargs :z-labels ())
  )

(defgeneric graph-3d-p (x)
  (:method ((x graph-3d)) t)
  (:method ((x t)) nil))


;;;===========================================================================
;;; Protocol.

;;; Constructor.

(defgeneric make-graph (type &rest keys &key &allow-other-keys))


;;; Extremes.

(defgeneric graph-max (graph coord))
(defgeneric graph-x-max (graph))
(defgeneric graph-y-max (graph))
(defgeneric graph-z-max (graph))

(defgeneric graph-min (graph coord))
(defgeneric graph-x-min (graph))
(defgeneric graph-y-min (graph))
(defgeneric graph-z-min (graph))


;;; Dataset handling.

(defgeneric add-dataset (graph dataset &key &allow-other-keys))

(defgeneric delete-dataset (graph dataset &key test key))

(defgeneric clear-graph (graph))

(defgeneric graph-n-datasets (graph))

(defgeneric get-dataset (graph selector))


;;; PLot Component protocol.
;;; Maybe move it in the '-implementation' file.

(defmethod name-of ((g graph)) (graph-name g))

(defmethod part-of ((g graph)) (in-frame g))

(defmethod parent ((g graph)) (in-frame g))

(defmethod represents ((g graph)) nil)

(defmethod representation ((g graph)) nil)

(defmethod components ((g graph)) (graph-datasets g))


;;; end of file -- graph.lisp --
