;;;; -*- Mode: Lisp -*-

;;;; utilities.lisp
;;;;
;;;; Plotting utilities for LW; various utility functions and macros.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(in-package "CL-PLOT")

(defmacro multiple-value-select (form &rest selectors)
  "Selects a few of the values returned by FORMS.

The values in the positions contained in SELECTORS are returned."

  (let ((list-var (gensym "MVS-VAR-"))
        (selector-var (gensym "MVS-SELECTOR-VAR-"))
        )
    `(let ((,list-var (multiple-value-list ,form)))
       (declare (dynamic-extent ,list-var))
       (apply #'values
              (mapcar #'(lambda (,selector-var)
                          (nth ,selector-var ,list-var))
                      (list ,@selectors))))
    ))
              
;;;; end of file -- utilities.lisp
