;;;; -*- Mode: Lisp -*-

(in-package "CL-PLOT")

;;;; axis.lisp
;;;;
;;;; Plotting utilities for Common Lisp; "axis" definitions w.r.t. a "dataset".
;;;;
;;;; See the file COPYING for copyright and licensing information.

(in-package "CL-PLOT")


;;; cartesian-axes --

(deftype cartesian-axes ()
  '(member :x :y :z))


;;; axis-kind --

(deftype axis-kind ()
  '(member integer real symbol) ; Where 'symbol' means "categorical"
                                ; (could be strings).
  )


;;; axis --

(defclass axis ()
   ((coord :accessor axis-coord
           :initarg :coordinate
           :initarg :coord
           :type cartesian-axes)

    (data :accessor axis-data
          :initarg :data
          :type (or null list vector graph dataset)
          )

     ;; Appearance and styling slots.

    (axis-kind :reader axis-kind
               :initform 'real
               :initarg :kind
               :type axis-kind)


    ;; Computed slots.

    (axis-labels :reader axis-labels
                 :writer set-axis-labels
                 :initarg :labels
                 )

    (n-ticks :accessor axis-n-ticks
             :initarg :n-ticks
             )

    ;; Plot components support.

    (name :accessor axis-name
          :initarg :name)

    (in-frame :accessor in-frame
              :initarg :in-frame)
    )

   (:default-initargs
    :name (gensym "AXIS-")
    :in-frame nil)
   )

(defgeneric axis-p (x)
  (:method ((x axis)) t)
  (:method ((x t)) nil))

(defun is-axis (x) (axis-p x))


;;; Axis protocol.
;;; --------------

(defgeneric compute-ticks (axis &key &allow-other-keys))

(defgeneric format-ticks (axis &key label-format &allow-other-keys))


;;; PLot Component protocol.
;;; Maybe move it in the '-implementation' file.

(defmethod name-of ((a axis)) (axis-name a))

(defmethod part-of ((a axis)) (in-frame a))

(defmethod parent ((a axis)) (in-frame a))

(defmethod represents ((a axis)) (axis-data a))

(defmethod representation ((a axis)) (axis-data a))

(defmethod components ((a axis)) (axis-labels a))


;;;; end of file -- axis.lisp --
