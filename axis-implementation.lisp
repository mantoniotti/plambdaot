;;;; -*- Mode: Lisp -*-

(in-package "CL-PLOT")

;;;; axis-implementation.lisp
;;;;
;;;; Plotting utilities for Common Lisp; "axis" protocol implementation.
;;;;
;;;; See the file COPYING for copyright and licensing information.


;;; Axis protocol.
;;; --------------

(defmethod compute-ticks ((a axis) &key &allow-other-keys)
  (compute-data-ticks a (axis-data a)))


(defmethod compute-data-ticks ((a axis) (data sequence) &key &allow-other-keys)
  (multiple-value-bind (low low-offset high high-offset step step-mag)
      (seq-min-max-bounds data)
    (declare (type real low low-offset high high-offset step)
             (type fixnum step-mag)
             (ignorable low-offset high-offset))

    (let ;; ((span (- high low)))
        ((span (- (- high high-offset) (+ low low-offset))))
      (declare (type real span))
      (flet ((tick-sequence (increment)
               (declare (type real increment))
               (loop for x of-type real
                       ;; from low by increment
                       from (+ low low-offset) by increment
                     ;; collect x
                     ;; while (<= x high)
                     while (<= x (- high high-offset))
                     collect x
                     ))
             )
      
        (if (not (slot-boundp a 'n-ticks))
            (tick-sequence step)

            (let* ((n-intervals (axis-n-ticks a))
                   (interval
                    (choose-interval ;; (floor span n-intervals)
                                     (/ span n-intervals)
                                     step
                                     step-mag))
                   )
              ;; (values n-intervals interval)
              (tick-sequence interval)
              ))
        ))))


(defmethod compute-data-ticks ((a axis) (data dataset-2d) &key &allow-other-keys)
  (with-slots (coord) a
    (compute-data-ticks a
                        (ecase coord
                          (:x (dataset-x-data data))
                          (:y (dataset-y-data data))))))


(defmethod compute-data-ticks ((a axis) (data dataset-3d) &key &allow-other-keys)
  (values (compute-data-ticks a (dataset-x-data data))
          (compute-data-ticks a (dataset-y-data data)))
  )


(defmethod compute-data-ticks ((a axis) (data dataset) &key &allow-other-keys)
  ;; Catch all method; not necessarily correct.
  (warn "CL-PLOT: computing data tick positions for a amorphous dataset (~S)."
        data)
  (compute-data-ticks a (dataset-raw-data data))
  )


(defmethod compute-data-ticks ((a axis) (data graph) &key &allow-other-keys)
  (error "CL-PLOT: unimplemented."))


(defmethod compute-data-ticks ((a axis) (data graph-2d) &key &allow-other-keys)
  (let ((ds (graph-datasets data)))
    (declare (type list ds))
    (cond ((null ds) ()) ; Probably an error.
          ((null (rest ds)) ; Just one dataset.
           (compute-data-ticks a (first ds)))
          (t
           ;; Let's try to be smart.
           ;;
           ;; 20230323: well: maybe this wasn't very smart.
           (loop for d of-type dataset in ds
                 for d-ticks of-type list = (compute-data-ticks a d)
                 minimize (first d-ticks) into ds-min
                 maximize (first (last d-ticks)) into ds-max
                 finally (return (compute-data-ticks a (list ds-min ds-max)))))
          )))


(defun choose-interval (raw-interval step step-mag)
  (declare (type real raw-interval step)
           (type fixnum step-mag))

  (let ((raw-interval-mag (magnitude raw-interval))
        (raw-interval-type (type-of raw-interval))
        )
    (declare (type fixnum raw-interval-mag))

    (if (<= raw-interval step)
        (cond ((or (= raw-interval-mag +dfem+)
                   (= step-mag +dfem+))
               raw-interval) ; ... or STEP, whatever.
              ((= raw-interval-mag step-mag)
               step) ; This will work.
              ((< raw-interval-mag step-mag)
               (expt (if (subtypep raw-interval-type 'rational)
                         10
                         (coerce 10 raw-interval-type))
                     raw-interval-mag))
              (t (error "Cannot choose interval.")))

        ;; (> raw-interval step)
        ;; Try to be more accomodating.  Note the ROUND, which may increase the step.
        (* (round raw-interval (expt 10 raw-interval-mag))
           (expt (if (subtypep raw-interval-type 'rational)
                     10
                     (coerce 10 raw-interval-type))
                 raw-interval-mag)
           ))))


(defparameter *axis-label-format-control* "~9,2f")
  
(defmethod format-ticks ((a axis)
                         &key
                         (label-format *axis-label-format-control*)
                         &allow-other-keys)
  (format-data-ticks a (axis-data a) :label-format label-format)
  )


(defmethod format-data-ticks ((a axis) (data sequence)
                              &key
                              (label-format *axis-label-format-control*)
                              &allow-other-keys)
  (loop for tick in (compute-data-ticks a data)
        collect (format nil label-format tick)))


(defmethod format-data-ticks ((a axis) (data dataset-2d)
                              &key
                              (label-format *axis-label-format-control*)
                              &allow-other-keys)
  (with-slots (coord) a
    (format-data-ticks a
                       (ecase coord
                         (:x (dataset-x-data data))
                         (:y (dataset-y-data data)))
                       :label-format label-format))
  )


(defmethod format-data-ticks ((a axis) (data dataset-3d)
                              &key
                              (label-format *axis-label-format-control*)
                              &allow-other-keys)

  (values (format-data-ticks a (dataset-x-data data) :label-format label-format)
          (format-data-ticks a (dataset-y-data data) :label-format label-format))
  )


(defmethod format-data-ticks ((a axis) (data dataset)
                              &key
                              (label-format *axis-label-format-control*)
                              &allow-other-keys)
  ;; Catch all method; not necessarily correct.
  (warn "CL-PLOT: formatting data tick labels for a amorphous dataset (~S)."
        data)

  (format-data-ticks a (dataset-raw-data data) :label-format label-format)
  )



(defmethod format-data-ticks ((a axis) (data graph)
                              &key
                              (label-format *axis-label-format-control*)
                              &allow-other-keys)
  (declare (ignore label-format))
  (error "CL-PLOT: unimplemented."))


(defmethod format-data-ticks ((a axis) (data graph-2d)
                              &key
                              (label-format *axis-label-format-control*)
                              &allow-other-keys)
  (loop for tick in (compute-data-ticks a data)
        collect (format nil label-format tick)))


;;;; end of file -- axis-implementation.lisp --
