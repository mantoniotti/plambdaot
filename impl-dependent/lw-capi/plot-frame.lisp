;;; -*- Mode: Lisp -*-

;;;; plot-frame.lisp
;;;;
;;;; Plotting utilities for LW; CAPI devices and pinboard objects.
;;;;
;;;; See the file COPYING for copyright and licensing information.


(in-package "CL-PLOT-CAPI")

;;; Notes:
;;; Each plot is displayed on a pinboad-layout.  The actual widget
;;; hierarchy is going to be the following:
;;;
;;; capi-device (a pinboard-layout)
;;;  +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-object (a pinboard-object, i.e. the actual plot)
;;;       +---> plot-x-axis (a pinboard-object)
;;;       +---> plot-y-axis (a pinboard-object)

;;; 20230201 MA:
;;; It was:
;;;
;;; +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-area (a simple-pinboard-layout)
;;;             +---> plot-object (a pinboard-object, i.e. the actual plot)
;;;       +---> plot-x-axis (a pinboard-object)
;;;       +---> plot-y-axis (a pinboard-object)


;;; Geometry of PLOT-FRAME
;;; ====================================================================
;;;
;;; A PLOT-FRAME must accommodate
;;; 1. The plot object
;;; 2. The plot axes
;;;
;;; It also must accommodate other items, e.g., the legend.
;;; To this end it must be clear what is the 'geometry' of the
;;; PLOT-FRAME, which then rely on the CAPI actual geometry
;;; quantities. 
;;; Maybe, in the end, a PLOT-FRAME will become a different kind of
;;; layout.
#|

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ margin                                                   +
+  ++++++++++++++++++++++++++++++++++++++++++++++++++++++  +
+  + axis                                               +  +
+  +  ++++++++++++++++++++++++++++++++++++++++++++++++  +  +
+  +  + plot object                                  +  +  +
+  +  +                                              +  +  +
+  +  +                                              +  +  +
+  +  +                                              +  +  +
+  +  ++++++++++++++++++++++++++++++++++++++++++++++++  +  +
+  +                                                    +  +
+  ++++++++++++++++++++++++++++++++++++++++++++++++++++++  +
+                                                          +
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

|#
;;; Margin and axis can (and will) be split into left, right, top and
;;; bottom.


;;; Definitions.
;;;---------------------------------------------------------------------

(defparameter *plot-frame-operations* (list :select :zoom))


;;; plot-frame --

(defclass plot-frame (pinboard-layout
                      bordered-output-pane) ; Maybe it could also be a PINBOARD-OBJECT.
  ((viewport-x :accessor viewport-x :initform 0.0)
   (viewport-y :accessor viewport-y :initform 0.0)
   (transform :accessor plot-frame-transform
              :initform (gp:make-transform))

   ;; CURRENT-ZOOM-FACTOR is the scaling factor currently used.
   ;; PREVIOUS-ZOOM-FACTOR is the scaling factor previously used.
   ;; ZOOM-DELTA is the increment (or decrement) used with zooming
   ;; operations.
   (current-zoom-factor :accessor current-zoom-factor :initform 1.0)
   (previous-zoom-factor :accessor previous-zoom-factor :initform 1.0)
   (zoom-delta :accessor zoom-delta :initform 0.1)

   (current-operation :accessor current-operation
                      :initform :select
                      ;; One of the operations listed in *plot-frame-operations*
                      )

   (tick-size :accessor tick-size
              :initform 10 ; pixels.
              )
   (margin :accessor margin
           :initform 10
           :initarg :margin)
   )
  (:documentation "The Plot Frame Class.")
  )


#+(or lispworks7 lispworks8)
(defmethod initialize-instance :after ((pf plot-frame) &key &allow-other-keys)
  (gp:set-graphics-state pf :scale-thickness nil)
  )


(defgeneric plot-frame-p (x)
  (:method ((x plot-frame)) t)
  (:method ((x t)) nil))


;;;---------------------------------------------------------------------------
;;; Implementation (to be put elsewhere)

(defun plot-frame-device (plot-frame)
  (declare (type plot-frame plot-frame))
  (element-parent plot-frame))

(defun plot-object-frame (plot-object)
  (declare (type plot-object plot-object))
  (element-parent plot-object))

(defun plot-object-device (plot-object)
  (declare (type plot-object plot-object))
  (element-parent (element-parent plot-object)))

(defun capi-device-plot-frame (capi-device)
  (declare (type capi-device capi-device))
  (find-if #'plot-frame-p (layout-description capi-device)))


;;; plot-frame-box

(declaim (ftype (function (plot-frame)
                          (values integer integer integer integer))
                plot-frame-box)

         (inline plot-frame-box))

(defun plot-frame-box (frame)
  "Returns the X, Y, W and H of a PLOT-FRAME."

  (declare (type plot-frame frame))

  ;; The function is just a wrapper around
  ;; capi:static-layout-child-geometry, but it is here for (semi) back
  ;; compatability.

  (static-layout-child-geometry frame))


#|
(defun plot-frame-box (drawing-area)
  "Returns the X, Y, W and H of a plot frame within DRAWING-AREA.

The function returns four values. DRAWING-AREA is, e.g., a CAPI-DEVICE."

  (declare (type capi-device drawing-area))

  (multiple-value-bind (min-width min-height)
      (get-constraints drawing-area) ; (values 10 10)

    (assert (and min-width min-height) (min-width min-height))
    
    (with-geometry drawing-area
      (let* ((w (or %width% min-width))
             (h (or %height% min-height))
             (plot-frame-w (round (* w 0.8)))
             (plot-frame-h (round (* h 0.8)))
             (plot-frame-x (round (/ (- w plot-frame-w) 2.0)))
             (plot-frame-y (round (/ (- h plot-frame-h) 2.0)))
             )
        (values plot-frame-x plot-frame-y
                plot-frame-w plot-frame-h)
        ))))
|#

;;; end of file -- plot-frame.lisp --
