;;;; -*- Mode: Lisp -*-

;;;; bordered-output-pane.lisp
;;;;
;;;; See the file COPYING in top folder for copyright and licensing
;;;; information.

(in-package "CL-PLOT-CAPI")


(defclass bordered-output-pane (output-pane)
  ((border-width :initarg :border-width
                 :initform 1
                 :accessor border-width
                 :accessor border-thickness
                 )
   )
  )


(defmethod border-width ((op output-pane)) 2)

(defmethod border-thickness ((op output-pane)) 2)

(defun draw-border (output-pane &key
                                (x 0 xp)
                                (y 0 yp)
                                (w 1 wp)
                                (h 1 hp)
                                filled
                                (color :blue)
                                (thickness (border-thickness output-pane))
                                &allow-other-keys)
  (with-geometry output-pane
    (let ((x (if xp x %x%))
          (y (if yp y %y%))
          (w (if wp w %width%))
          (h (if hp h %height%))
          )
      #+nil
      (gp:draw-string output-pane
                      (format nil "D-B ~S ~S"
                              (list x xp y yp w wp h hp)
                              (list %x% %y% %width% %height%))
                      (+ x 50)
                      (+ y 50))
                      
      (gp:draw-rectangle output-pane x y (1- w) (1- h)
                         :foreground color
                         :filled filled
                         :thickness thickness))))

;;;; end of file -- bordered-output-pane.lisp --
