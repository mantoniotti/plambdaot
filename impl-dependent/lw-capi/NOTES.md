# LW CAPI Implementations Notes

* 20230105: MA  
  Zoom works but positioning and zooming out produce sub-optimal
  effects.  More honing is required.
  Also, the toolbar buttons maybe should not zoom as they are "operations".

* 20230201: MA
  Changed naming of library.  Changing now layout of plot elements due
  to z-layer drawing issues among pinboard layouts.  Axes and graphs
  should be drawn on the same layout.  
  The new hierarchy (for 2d plots) is thus:
  
  capi-device (a pinboard-layout)
  +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
          +---> plot-object (a pinboard-object, i.e. the actual plot)
          +---> plot-x-axis (a pinboard-object)
          +---> plot-y-axis (a pinboard-object)

