;;;; -*- Mode: Lisp -*-

;;;; colors.lisp --
;;;; Colors utilities
;;;;
;;;; See the file COPYING in top folder for copyright and licensing information.
;;;;
;;;; Notes:
;;;;
;;;; 20221130 MA: created.


(in-package "CL-PLOT-CAPI")

;;; *colors*
;;;
;;; Just ensure that the "main" colors are chosen first.

(defparameter *colors*
  (append  '(:black :red :blue :yellow :green :brown :gray)
           (set-difference (color:get-all-color-names)
                           '(:black
                             :red
                             :blue
                             :yellow
                             :green
                             :brown
                             :gray)
                           :test #'eq)))


(defparameter *n-colors* (list-length *colors*))


(defun choose-color (&optional (i 0))
  (declare (type fixnum i))
  (nth (mod i *n-colors*) *colors*))

;;;; end of file -- colors.lisp --
