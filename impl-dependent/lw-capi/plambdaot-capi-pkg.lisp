;;;; -*- Mode: Lisp -*-

;;;; plambdaot-capi-pkg.lisp
;;;;
;;;; Plotting utilities for LW; CAPI package definition.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(defpackage "IT.UNIMIB.DISCO.MA.PLAMBDAOT.CAPI"
  (:use "COMMON-LISP" "PLAMBDAOT" "CAPI")

  (:documentation
   "The PLambdaOT CAPI package.

Lispworks CAPI backend for PLambdaOT.")

  (:nicknames
   "PLAMBDAOT.CAPI"
   "PLAMBDAOT-CAPI"
   "CL-PLOT-CAPI" #| Backward compatability |#)

  (:export
   "PLOT-INTERFACE"
   "CAPI-DEVICE")

  (:import-from "PLAMBDAOT"
   "DATASET-RAW-DATA")

  (:import-from "PLAMBDAOT"

   ;; Axis names/slots used in this package.

   "AXIS"
   "AXIS-P"
   "IS-AXIS"
   "AXIS-KIND"
   "CARTESIAN-AXES"

   "COORD"
   "DATA"
   "AXIS-LABELS"
   "N-TICKS"
   )
  )

;;;; end of file -- plambdaot-capi-pkg.lisp --
