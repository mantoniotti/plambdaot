;;; -*- Mode: Lisp -*-

;;;; gestures.lisp --
;;;; Based on code by Eduardo Munoz.
;;;;
;;;; See the file COPYING for copyright and licensing information.



(in-package "CL-PLOT-CAPI")

(defgeneric invoke-plot-frame-command (command plot-frame &rest args)
  (:method ((plot-frame plot-frame) (command t) &rest args)
   (warn "Unknown command ~S invoked with plot frame ~S."
         command plot-frame)))


(defmethod invoke-plot-frame-command ((command (eql :select))
                                      (plot-frame plot-frame)
                                      &rest args)
  ;; A no-op.
  (declare (ignore args))
  )


(defmethod invoke-plot-frame-command ((command (eql :zoom))
                                      (plot-frame plot-frame)
                                      &rest args)
  ;; A no-op.
  (apply #'zoom-button plot-frame args)
  )


;; Set initial coordinates of mouse drag. start-x and
;; start-y are slots of the device (they used to be of the interface).

(defun press-button (pinboard x y)
  (let ((device (element-parent pinboard))
        (current-transform (gp:graphics-port-transform pinboard))
        )

    (declare (ignore current-transform))

    (log-message "press-button ~S ~S.~%" pinboard (current-operation pinboard))

    (setf (button-pressed-p device) t
          (start-x device) x
          (start-y device) y
          ;; (viewport-x device) (nth 4 current-transform)
          ;; (viewport-y device) (nth 5 current-transform)
          ;; (current-scale device) (first current-transform)
          )
    (invoke-plot-frame-command (current-operation pinboard) pinboard x y)))


(defun release-button (pinboard x y)
  (declare (ignore x y))

  (let ((device (capi:element-parent pinboard)))
    (setf (button-pressed-p device) nil)))


;; button-1 pans

(defun drag-button-1 (pinboard x y)

  (log-message "drag-button-1 ~S ~S ~S~%" pinboard x y)

  (let* ((device (capi:element-parent pinboard))
         (dist-x (- x (start-x device)))
         (dist-y (- y (start-y device)))
         (transform (gp:copy-transform (gp:graphics-port-transform pinboard)))
         )
    (gp:apply-translation transform dist-x dist-y)
    (setf (gp:graphics-port-transform pinboard) transform
          (start-x device) x
          (start-y device) y)

    ;; call your redraw function here

    (dolist (po (layout-description pinboard))
      (gp:invalidate-rectangle po))))




(defun reset-zoom (plot-frame)
  (declare (type plot-frame plot-frame))
  (setf (current-zoom-factor plot-frame) 1.0
        (current-scale plot-frame) 1.0))


#+old-zoom-scheme
(defun zoom-button (plot-frame x y)
  (declare (type plot-frame plot-frame))
  (with-accessors ((current-scale current-scale)
                   (current-zoom-factor current-zoom-factor)
                   )
      plot-frame

    ;; Ensure zooming out would not zoom too much.
    (when (> 1.0 (* current-zoom-factor current-scale))
      (setf current-zoom-factor 1.0
            (plot-frame-transform plot-frame) (gp:make-transform)
            x 0 y 0))

    (with-geometry plot-frame
      (gp:with-graphics-scale (plot-frame %width% %height%)
        (let ((new-tr (plot-frame-transform plot-frame))
              (current-tr (make-scaling-transform %width% %height%))
              )
          (declare (dynamic-extent current-tr))
          (gp:postmultiply-transforms current-tr new-tr)
          (multiple-value-bind (zero-x zero-y)
              (gp:transform-point current-tr 0 0)

            (let ((min-x (- zero-x (- zero-x x)))
                  (min-y (- zero-y (- zero-y y)))
                  )
              (setf new-tr (centering-zoom min-x min-y current-zoom-factor new-tr)
                    current-scale (* current-zoom-factor current-scale)
                    (viewport-x plot-frame) min-x
                    (viewport-y plot-frame) min-y)

              ;; Setf the new trasform.
              (setf (plot-frame-transform plot-frame) new-tr)
                
              ;; Make sure everything is redrawn.
              (gp:invalidate-rectangle plot-frame)
              (gp:invalidate-rectangle (element-parent plot-frame))))))
      )))


;;; New zoom scheme.

(defun zoom-button (plot-frame x y)

  (declare (type plot-frame plot-frame)
           (ignore x y))

  (with-accessors ((current-scale current-scale)
                   (current-zoom-factor current-zoom-factor)
                   )
      plot-frame

    ;; Ensure zooming out would not zoom too much.

    (when (> 1.0 (* current-zoom-factor current-scale))
      (setf current-zoom-factor 1.0))

    (setf current-scale (* current-zoom-factor current-scale))

    ;; Make sure everything is redrawn.

    (gp:invalidate-rectangle plot-frame)
    (gp:invalidate-rectangle (element-parent plot-frame))
    ))
    

(defun centering-zoom (dx dy zoom tr)
  (gp:apply-translation tr (- dx) (- dy))
  (gp:apply-scale tr zoom zoom)
  (gp:apply-translation tr dx dy)
  tr)

;;; end of file -- gestures.lisp --
