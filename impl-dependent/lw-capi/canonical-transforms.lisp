;;;; -*- Mode: Lisp -*-

;;;; canonical-transforms.lisp
;;;;
;;;; See the file COPYING in top folder for copyright and licensing
;;;; information.


(in-package "CL-PLOT-CAPI")


(defun make-id-transform ()
  (gp:copy-transform gp:*unit-transform*))


(defun make-translation-transform (&optional (dx 0) (dy 0))
  (declare (type real dx dy))
  (gp:make-transform 1 0 0 1 dx dy))


(defun make-scaling-transform (&optional (scale-x 1) (scale-y 1))
  (declare (type real scale-x scale-y))
  (gp:make-transform scale-x 0 0 scale-y))


#|
(defun make-rotation-transform (&optional (s 0) (c 1))
  (gp:make-transform c (- s) s c))
|#


(defun make-rotation-transform (&optional (angle 0))
  (declare (type real angle)) ; In radians
  (let ((s (sin angle))
        (c (cos angle))
        )
    (declare (type (real -1.0d0 1.0d0) s c))
    (gp:make-transform  c  s (- s) c 0 0)))


(defun make-x-reflect-transform ()
  (gp:make-transform 1 0 0 -1))


(defun make-y-reflect-transform ()
  (gp:make-transform -1 0 0 1))


(declaim (inline transform-x-translation
                 transform-y-translation
                 transform-x-scale
                 transform-y-scale))

(defun transform-x-translation (transform)
  (fifth transform))

(defun transform-y-translation (transform)
  (sixth transform))

(defun transform-x-scale (transform)
  (first transform))

(defun transform-y-scale (transform)
  (fourth transform))



(defun ortho-2d (left
                 right
                 top
                 bottom
                 &optional
                 (transform
                  (gp:copy-transform gp:*unit-transform*)))

  (declare (type real left right top bottom)
           (type gp:transform transform))

  (let ((ortho-2d-transform
         (gp:make-transform (/ 2.0 (- right left))
                            0.0
                            0.0
                            (/ 2.0 (- bottom top))
                            (coerce (- (/ (+ right left)
                                          (- right left)))
                                    'single-float)
                            (coerce (- (/ (+ top bottom)
                                          (- bottom top)))
                                    'single-float)))
        )
    (declare (dynamic-extent ortho-2d-transform))
    (gp:postmultiply-transforms transform ortho-2d-transform)
    transform))


(defun viewport (x
                 y
                 width
                 height
                 &optional
                 (transform (gp:copy-transform gp:*unit-transform*)))

  (declare (type real x y width height)
           (type gp:transform transform))

  (let ((w/2 (/ width 2.0))
        (h/2 (/ height 2.0))
        )
    (gp:postmultiply-transforms transform
                                (gp:make-transform w/2
                                                   0
                                                   0
                                                   h/2
                                                   (+ w/2 x)
                                                   (+ h/2 y)))
    transform))


(defmacro with-projection-transformations
          ((drawing-area
            &optional
            (transform
             `(plot-frame-transform ,drawing-area)))
           &body forms)
  `(with-geometry ,drawing-area
     (gp:with-graphics-transform (,drawing-area ,transform)
       (gp:with-graphics-scale (,drawing-area %width% %height%)
         ,@forms))))
    

;;;; end of file -- canonical-transforms.lisp --
