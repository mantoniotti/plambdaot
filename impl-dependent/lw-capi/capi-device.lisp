;;; -*- Mode: Lisp -*-

;;;; capi-device.lisp
;;;;
;;;; Plotting utilities for LW; CAPI devices and pinboard objects.
;;;;
;;;; See the file COPYING for copyright and licensing information.


(in-package "CL-PLOT-CAPI")

;;; Note:
;;; Each plot is displayed on a pinboad-layout.  The actual widget
;;; hierarchy is going to be the following:
;;;
;;; capi-device (a pinboard-layout)
;;; +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-area (a simple-pinboard-layout)
;;;             +---> plot-object (a pinboard-object, i.e. the actual plot)
;;;       +---> plot-x-axis (a pinboard-object)
;;;       +---> plot-y-axis (a pinboard-object)


;;; Definitions.
;;; --------------------------------------------------------------------


;;; capi-device --

(defclass capi-device (device pinboard-layout)
  ((start-x :accessor start-x :initform 0)
   (start-y :accessor start-y :initform 0)

   ;; start-x and start-y are the mouse clicks positions.

   (viewport-x :accessor viewport-x :initform 0)
   (viewport-y :accessor viewport-y :initform 0)

   (button-pressed-p :accessor button-pressed-p :initform nil)

   )
  (:documentation "The CL-PLOT CAPI Device Class.")
  )

(defgeneric capi-device-p (x)
  (:method ((x capi-device)) t)
  (:method ((x t)) nil))


;;; end of file -- capi-device.lisp --
