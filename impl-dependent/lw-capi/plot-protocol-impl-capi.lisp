;;;; -*- Mode: Lisp -*-

;;;; plot-protocol-impl-capi.lisp --
;;;; Cleaned up version of CAPI plot protocol implementation.
;;;;
;;;; See the file COPYING in top folder for copyright and licensing information.


(in-package "CL-PLOT-CAPI")

;;; Notes:
;;;
;;; Each plot is displayed on a pinboad-layout.  The actual widget
;;; hierarchy is going to be the following:
;;;
;;; capi-device (a pinboard-layout)
;;;  +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-object (a pinboard-object, i.e. the actual plot)
;;;       +---> plot-x-axis (a pinboard-object)
;;;       +---> plot-y-axis (a pinboard-object)
;;;
;;; 20230201 MA:
;;; It was:
;;;
;;; capi-device (a pinboard-layout)
;;; +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-area (a simple-pinboard-layout)
;;;             +---> plot-object (a pinboard-object, i.e. the actual plot)
;;;       +---> plot-x-axis (a pinboard-object)
;;;       +---> plot-y-axis (a pinboard-object)

;;;;
;;;; 20220505 MA: cleaned up version.
;;;;
;;;; 20220606 MA: almost working version.
;;;;
;;;; 20221110 MA: cleaned up version; almost working.
;;;;
;;;; 20230201 MA: New version in new package.


;;; A graph contains a list of datasets, which contain its own x and y
;;; (and z) data.
;;; This will allow a plot to include multiple datasets.


;;; Prologue.
;;; ---------

(defparameter *debug-draw-pb-borders* t
  "If non NIL draws a border around the pinboard object.")


;;; Protocol and useful interface.
;;; ------------------------------

(defmethod cl-plot::new-plot-display ((s (eql :figure))
                                      &rest keys
                                      &key
                                      (show t)
                                      (set-as-output t)
                                      &allow-other-keys)
  (declare (ignore show set-as-output))
  (apply #'new-plot-interface keys))


(defgeneric resize-plot-device-object (plot-device plot-device-object x y w h))

(defgeneric erase-plot-device (plot-device))

(defgeneric erase-plot-device-object (plot-device plot-device-object x y w h))


;;;---------------------------------------------------------------------------
;;; plot-data
;;;
;;; The basic method is
;;;
;;;    PLOT-DATA <capi-device> <graph-2d>
;;;
;;; The other methods essentially transform the inputs or select their
;;; inside bits to direct the plotting action.
;;; In particular, PLOT-DATA <capi-device> <dataset-2d> first creates
;;; a <graph-2d>, then adds the dataset to is and finally calls the
;;; basic method.


;;; plot-data plot-interface dataset

(defmethod plot-data ((plot-interface plot-interface)
                      (d2d dataset-2d)
                      &rest keys
                      &key
                      (name "")
                      (x 50)
                      (y 20)
                      (width 300)
                      (height 200)
                      (n-ticks 10)
                      (tick-size 10)
                      )
  (declare (ignore name x y width height n-ticks tick-size))
  (let ((new-graph (make-instance 'graph-2d)))
    (add-dataset new-graph d2d)
    (apply #'plot-data plot-interface new-graph keys)))


(defmethod plot-data :after ((plot-interface plot-interface)
                             dataset
                             &rest keys
                             &key)
  (declare (ignore keys))
  (capi:display plot-interface))


;;; plot-data plot-interface graph

(defmethod plot-data ((plot-interface plot-interface)
                      (g graph-2d)
                      &rest keys
                      &key
                      &allow-other-keys)
  (apply #'execute-with-interface plot-interface
         #'plot-data (plot-interface-device plot-interface) g keys))


;;; plot-data capi-device graph
;;;
;;; This essentially constructs the PLOT-FRAME hierarchy for a GRAPH
;;; over a CAPI-DEVICE.
;;; See the definition of PLOT-FRAME anf PLOT-AREA for thir geometry
;;; description.
;;;
;;; Notes:
;;;
;;; 20230120 MA
;;; The choice of having the plot-area essentially a layout implies
;;; that it will always be drawn on top of the pinboard objects drawn
;;; on the plot-frame.  This means that the axis need to be nudged
;;; away from it, in order to draw them properly.  This also has
;;; implications for drawing grids over the plot.

#| Old version
(defmethod plot-data ((drawing-area capi-device)
                      (g graph-2d)
                      &key
                      (name (graph-name g))
                      (draw-legend-p t)
                      (draw-x-labels-p t)
                      (draw-y-labels-p t)
                      (tick-size 10)
                      (margin 10)
                      &allow-other-keys)
  (declare (ignore name))
  (multiple-value-bind (min-width min-height)
      (get-constraints drawing-area) ; (values 10 10)

    (assert (and min-width min-height) (min-width min-height))

    (with-geometry drawing-area
      (let* ((psw (gp:port-string-width drawing-area "-00000.00"))
             (psh (gp:port-string-height drawing-area "-00000.00"))

             (w (or %width% min-width))
             (h (or %height% min-height))
             (plot-frame-w (* w 0.8))
             (plot-frame-h (* h 0.8))
             (plot-frame-x (/ (- w plot-frame-w) 2.0))
             (plot-frame-y (/ (- h plot-frame-h) 2.0))

             (plot-area-w (- plot-frame-w (* 2 (+ margin psw))))
             (plot-area-h (- plot-frame-h (* 2 (+ margin psh))))
             (plot-area-x (+ margin psw))
             (plot-area-y (+ margin psh))

             (plot-x-axis-x plot-area-x)
             (plot-x-axis-y plot-area-h)

             (plot-y-axis-x margin)
             (plot-y-axis-y margin)

             (new-tr (gp:copy-transform gp:*unit-transform*))

             (new-plot-frame
              (make-instance 'plot-frame
                             :x plot-frame-x
                             :y plot-frame-y
                             :width plot-frame-w
                             :height plot-frame-h

                             :visible-min-width  plot-frame-w
                             :visible-min-height plot-frame-h
                             :visible-max-width  plot-frame-w
                             :visible-max-height plot-frame-h

                             :background :white
                             :input-model '(((:button-1 :press) press-button)
                                            ((:button-1 :release) release-button)
                                            )
                             :margin margin
                             ))

             (new-plot-area
              (make-instance 'plot-area
                             :x plot-area-x
                             :y plot-area-y
                             :background :white
                             :width  plot-area-w
                             :height plot-area-h
                             :internal-min-width  plot-area-w
                             :internal-min-height plot-area-h
                             :internal-max-width  plot-area-w
                             :internal-max-height plot-area-h
                             ))
             
             (new-plot-object
              (make-instance 'plot-object
                             :draw-legend-p   draw-legend-p
                             :draw-x-labels-p draw-x-labels-p
                             :draw-y-labels-p draw-y-labels-p
                             :tick-size tick-size
                             :data g
                             ))

             (plot-object-title
              (make-plot-object-title drawing-area g
                                      plot-frame-x plot-frame-y
                                      (1- plot-frame-w) (1- plot-frame-h)))

             (x-axis-object
              (make-plot-object-axis new-plot-object
                                     g
                                     :x
                                     plot-x-axis-x
                                     plot-x-axis-y
                                     plot-area-w
                                     plot-area-h
                                     :draw-labels-p draw-x-labels-p
                                     :tick-size tick-size))

             (y-axis-object
              (make-plot-object-axis new-plot-object
                                     g
                                     :y
                                     plot-y-axis-x
                                     plot-y-axis-y
                                     plot-area-w
                                     plot-area-h
                                     :draw-labels-p draw-y-labels-p
                                     :tick-size tick-size))
             )

        (setf (plot-frame-transform new-plot-frame) new-tr)

        (setf (layout-description drawing-area) (list new-plot-frame))

        (setf (layout-description new-plot-frame) (list new-plot-area))

        (manipulate-pinboard new-plot-area new-plot-object :add-top)

        (manipulate-pinboard new-plot-frame x-axis-object :add-top)
        (manipulate-pinboard new-plot-frame y-axis-object :add-top)

        ;; (manipulate-pinboard new-plot-area new-plot-object :add-top)

        (manipulate-pinboard drawing-area plot-object-title :add-top)

        ))))
|#


(defmethod plot-data ((drawing-area capi-device)
                      (g graph-2d)
                      &rest keys
                      &key
                      (name (graph-name g))
                      (draw-legend-p t)
                      (draw-x-labels-p t)
                      (draw-y-labels-p t)
                      (tick-size 10)
                      (margin 10)
                      (spacing 2)
                      &allow-other-keys)
  (declare (ignorable name
                      draw-legend-p
                      draw-x-labels-p
                      draw-y-labels-p
                      tick-size
                      margin
                      spacing))

  (let* ((hierarchy
          (multiple-value-list
           (apply #'plot-setup drawing-area :graph-2d g keys)))
         (plot-obj (third hierarchy))
         )
    (declare (type list hierarchy)
             (type plot-object plot-obj))

    ;; (format t ">>> ~S~%>>> ~S~2%" plot-obj hierarchy)

    (setf (plot-object-data plot-obj) g)
    
    (values hierarchy plot-obj) ; Lost most of the times, as
                                ; PLOT-DATA is called via
                                ; EXECUTE-WITH-INTERFACE.
    ))


;;; make-plot-object-title

(defmethod make-plot-object-title ((drawing-area capi-device)
                                   (g graph-2d)
                                   x y w h)
  ;; x y w and h are the position and the dimensions of the PLOT-FRAME
  ;; in DRAWING-AREA. 

  (let* ((title (string (name g)))
         (title-psw (gp:port-string-width drawing-area title))
         (title-psh (gp:port-string-height drawing-area title))
         )
    (make-instance 'item-pinboard-object
                   :text title
                   :x (+ x (- (/ w 2.0) (/ title-psw 2.0)))
                   :y (- y 5 title-psh)
                   )
    ))
    

;;; resize-plot-device-object

(defmethod resize-plot-device-object ((drawing-area capi-device)
                                      (pbo pinboard-object)
                                      x y w h)
  (declare (ignore x y w h))
  ;; No op.
  ;; Essentially for the item-pinboard-object's now added.
  )


(defmethod resize-plot-device-object ((drawing-area capi-device)
                                      (plot plot-frame)
                                      x y w h)
  (declare (ignore x y w h))

  (let* ((plot-object (first (layout-description plot))) ; Quite an assumption!
         (x-axis (second (layout-description plot))) ; Ditto.
         (y-axis (third (layout-description plot))) ; Ditto.
         )

    (with-geometry drawing-area

      (assert (and %x% %y% %width% %height%))

      (let* ((w %width%)
             (h %height%)
             (plot-frame-w (round (* w 0.8)))
             (plot-frame-h (round (* h 0.8)))
             (plot-frame-x (round (/ (- w plot-frame-w) 2.0)))
             (plot-frame-y (round (/ (- h plot-frame-h) 2.0)))

             (margin 10) ; Make this a parameter.
             (psw (gp:port-string-width drawing-area "-00000.00"))
             (psh (gp:port-string-height drawing-area "-00000.00"))

             (plot-object-w (- plot-frame-w (* 2 (+ margin psw))))
             (plot-object-h (- plot-frame-h (* 2 (+ margin psh))))
             (plot-object-x (+ margin psw))
             (plot-object-y (+ margin psh))

             (plot-x-axis-x plot-object-x)
             (plot-x-axis-y (+ plot-object-y plot-object-h))
             (plot-x-axis-w plot-object-w)
             (plot-x-axis-h (+ margin psh))

             (plot-y-axis-x margin)
             ;; (plot-y-axis-y margin)
             (plot-y-axis-y plot-object-y)
             (plot-y-axis-w psw)
             (plot-y-axis-h plot-object-h)
             ;; (plot-y-axis-h (+ plot-object-h psh))
             )

        #|(declare (ignore plot-x-axis-x plot-x-axis-y
                           plot-y-axis-x plot-y-axis-y))|#

        (set-geometric-hint plot :x plot-frame-x)
        (set-geometric-hint plot :y plot-frame-y)
        (set-geometric-hint plot :width plot-frame-w)
        (set-geometric-hint plot :visible-min-width plot-frame-w)
        (set-geometric-hint plot :visible-max-width plot-frame-w)
        (set-geometric-hint plot :height plot-frame-h)
        (set-geometric-hint plot :visible-min-height plot-frame-h)
        (set-geometric-hint plot :visible-max-height plot-frame-h)

        (set-geometric-hint plot-object :x plot-object-x)
        (set-geometric-hint plot-object :y plot-object-y)

        (set-geometric-hint plot-object :width plot-object-w)
        (set-geometric-hint plot-object :visible-min-width plot-object-w)
        (set-geometric-hint plot-object :visible-max-width plot-object-w)
        (set-geometric-hint plot-object :height plot-object-h)
        (set-geometric-hint plot-object :visible-min-height plot-object-h)
        (set-geometric-hint plot-object :visible-max-height plot-object-h)

        ;; See what looks better.  set-geometric-hint or set-hint-table.
        (set-hint-table x-axis
                        (list :x plot-x-axis-x
                              :y plot-x-axis-y
                              :visible-min-width plot-x-axis-w
                              :visible-max-width plot-x-axis-w))

        (set-geometric-hint y-axis :x plot-y-axis-x)
        (set-geometric-hint y-axis :y plot-y-axis-y)
        (set-geometric-hint y-axis :visible-min-height plot-y-axis-h)
        ;; (set-geometric-hint y-axis :visible-max-height plot-object-h)
        (set-geometric-hint y-axis :visible-max-height plot-y-axis-h)

        ;; We clean now.
        ;; We need to do it here because of how Windows handles the clearing.
        ;; Note also that gp:invalidate-rectangle-seems to work,
        ;; unlike gp:clear-graphics-port.
        
        #+nil
        (progn
          (gp:invalidate-rectangle plot-object
                                   plot-object-x plot-object-y
                                   plot-object-w plot-object-h)
          (gp:invalidate-rectangle x-axis
                                   plot-x-axis-x plot-x-axis-y
                                   plot-object-w nil)
          (gp:invalidate-rectangle y-axis
                                   plot-y-axis-x plot-y-axis-y
                                   nil plot-object-h)
          )

        (progn
          (gp:invalidate-rectangle plot-object)
          (gp:invalidate-rectangle x-axis)
          (gp:invalidate-rectangle y-axis)
          )
        )))
  )


;;; erase-plot-device

(defmethod erase-plot-device ((drawing-area capi-device))
  (gp:invalidate-rectangle drawing-area))


;;; erase-plot-device-object

(defmethod erase-plot-device-object ((drawing-area capi-device)
                                     (plot plot-object)
                                     x y w h)
  (declare (ignore x y w h))

  (with-geometry drawing-area
    (gp:draw-rectangle drawing-area
                       %x% %y% %width% %height%
                       :foreground (simple-pane-background drawing-area)
                       :filled t)
    ))


;;; Draw methods
;;; ------------

(defgeneric draw-graph (device device-object graph &key &allow-other-keys))
(defgeneric draw-dataset (plot-frame dataset &key &allow-other-keys))
(defgeneric draw-graph-title (plot-frame plot-object))

(defmethod name ((g graph)) (graph-name g))
(defmethod name ((d dataset)) (dataset-name d))

(defparameter *break-drawing* t)


;;; draw-pinboard-object methods.
;;; -----------------------------
;;;
;;; I need DRAW-PINBOARD-OBJECT (and REDRAW-) methods for each
;;; PINBOARD-LAYOUT object. That is, for PLOT-OBJECT and PLOT-AXIS.

#| To be removed
(defmethod draw-pinboard-object ((drawing-area plot-frame)
                                 (plot plot-object)
                                 &key x y width height
                                 &aux
                                 (margin 10)
                                 (psw (gp:port-string-width drawing-area "-00000.00"))
                                 (psh (gp:port-string-height drawing-area "-00000.00"))
                                 (czf (current-zoom-factor drawing-area))
                                 (device (element-parent drawing-area))
                                 )
  (declare (ignorable x y width height psw psh margin))

  ;; The new version does not need anything but the scale.
  ;; Will do 3D later.

  (multiple-value-bind (xx yy ww hh)
      (static-layout-child-geometry plot)
    (display-message "X ~S Y ~S W ~S H ~S~@
                      XX ~S YY ~S WW ~S HH ~S"
                     x y width height
                     xx yy ww hh
                     )
    )

  (with-atomic-redisplay (drawing-area)
    (with-geometry drawing-area
      
      (draw-border drawing-area :x 0 :y 0 :color :grey :thickness 18)

      (gp:draw-line drawing-area  x y width height
                    :foreground :blue
                    :thickness 3
                    :scale-thickness nil)
      (gp:draw-line drawing-area x height width y
                    :foreground :blue
                    :thickness 3
                    :scale-thickness nil)

      (gp:draw-line drawing-area 0 0 %width% %height% 
                    :foreground :red
                    :thickness 1
                    :scale-thickness nil)
      (gp:draw-line drawing-area 0 %height% %width% 0
                    :foreground :red
                    :thickness 1
                    :scale-thickness nil)
      

      (multiple-value-bind (pox poy pow poh)
          (plot-object-box plot)
        (gp:draw-line drawing-area pox poy pow poh
                      :foreground :forestgreen
                      :thickness 1
                      :scale-thickness nil)
        (gp:draw-line drawing-area pox poh pow poy
                      :foreground :forestgreen
                      :thickness 1
                      :scale-thickness nil)

        (gp:draw-line drawing-area #| (or x 0) (or y 0) |# x y (or width %width%) (or height %height%)
                      :foreground :cyan
                      :thickness 3
                      :scale-thickness nil)
        (gp:draw-line drawing-area #| (or x 0) |# x (or height %height%) (or width %width%) #| (or y 0) |# y
                      :foreground :cyan
                      :thickness 3
                      :scale-thickness nil)
        )

      (gp:with-graphics-scale
          (drawing-area czf czf)

        ;; draw-graph (not anymore; just draw-dataset)

        (with-slots (data)
            plot
          (draw-unit-grid drawing-area)
          (format t "PLAMBDAOT: plotting ~S on ~S~%"
                  data
                  drawing-area)
          (when data
            (draw-dataset drawing-area data)
            ;; (draw-graph drawing-area plot data)
            ))
        ))))
|#

(defmethod draw-pinboard-object ((drawing-area plot-frame)
                                 (plot plot-object)
                                 &key
                                 (x 0)
                                 (y 0)
                                 (width 1)
                                 (height 1)
                                 &aux
                                 (margin 10)
                                 (psw (gp:port-string-width drawing-area "-00000.00"))
                                 (psh (gp:port-string-height drawing-area "-00000.00"))
                                 (czf (current-zoom-factor drawing-area))
                                 )
  (declare (ignorable x y width height psw psh margin)
           (type integer x y width height margin)
           (type real czf)
           )

  ;; The new version does not need anything but the scale.
  ;; Will do 3D later.


  (with-atomic-redisplay (drawing-area)
    (with-geometry drawing-area
      
      (draw-border drawing-area :x 0 :y 0 :color :grey)

      (gp:with-graphics-scale
          (drawing-area czf czf)

        ;; draw-graph (not anymore; just draw-dataset)

        (with-slots (data)
            plot
          (draw-unit-grid drawing-area)
          (format t "PLAMBDAOT: plotting ~S on ~S~%"
                  data
                  drawing-area)
          (when data
            (draw-dataset drawing-area data)
            ;; (draw-graph drawing-area plot data)
            ;; (draw-zero-axes drawing-area plot data)
            ))
        ))))


(defmethod draw-pinboard-object :before ((drawing-area plot-frame)
                                         (plot plot-object)
                                         &key
                                         (x 0)
                                         (y 0)
                                         (width 1)
                                         (height 1)
                                         &allow-other-keys
                                         )

  (declare (type integer x y width height))

  ;; (display-message "Before X ~S Y ~S W ~S H ~S" x y width height)

  (when *debug-draw-pb-borders*
    (with-atomic-redisplay (drawing-area)

      #+nil
      (multiple-value-bind (xx yy ww hh)
          (static-layout-child-geometry plot)
        ;; (display-message "Before XX ~S YY ~S WW ~S HH ~S" xx yy ww hh)
        )

      (block draw-plot-object-wires
        (gp:draw-line drawing-area  x y (+ x width) (+ y height)
                      :foreground :grey
                      :dashed t
                      :thickness 1
                      :scale-thickness nil)

        (gp:draw-line drawing-area  x (+ y height) (+ x width) y
                      :foreground :grey
                      :dashed t
                      :thickness 1
                      :scale-thickness nil
                      )
    
        (gp:draw-rectangle drawing-area x y (1- width) (1- height)
                           :foreground :grey
                           :filled nil
                           :dashed t
                           :thickness (border-thickness drawing-area)
                           :scale-thickness nil
                           )
        )
      )))


;;; Axes are drawn on the PLOT-FRAME

(defmethod draw-pinboard-object ((plot-frame plot-frame)
                                 (plot-x plot-axis-x)
                                 &key
                                 (x 0)
                                 (y 0)
                                 (width 1)
                                 (height 1)
                                 )
  "Drawing the X axis object of a 2D plot on a PLOT-FRAME.

The drawing is done by translating 'right' and by scaling by the
actual 'width' of the plot."

  (declare (ignorable x y width height)
           (type integer x y width height))

  (with-slots ((g data)
               plot-object
               draw-labels-p
               tick-size
               )
      plot-x

    (with-geometry plot-frame
      (let* ((margin 10)

             (psw (gp:port-string-width plot-frame "-00000.00"))

             (psh (gp:port-string-height plot-frame "-00000.00"))

             (plot-width (- %width% (* 2 (+ margin psw))))

             (axis-labels (cl-plot::format-ticks plot-x))

             (n-ticks
              (if (slot-boundp plot-x 'n-ticks)
                  (plot-axis-n-ticks plot-x)
                  (1+ (length axis-labels))))

             ;; (labels-height psh)

             (labels-lengths
              (mapcar #'(lambda (l)
                          (gp:port-string-width plot-frame l))
                      axis-labels))

             (max-label-len (apply #'max labels-lengths))

             (labels-tot-len
              (+ (* n-ticks 5) (reduce #'+ labels-lengths))) 

             (dx (/ (- %width% max-label-len) n-ticks))
             ;; (dx (/ (- plot-width max-label-len) n-ticks))

             (czf (current-zoom-factor plot-frame))
             )

        (declare (type integer
                       psw psh max-label-len labels-tot-len
                       n-ticks margin)
                 (type real dx cfz)
                 (type list labels-lengths axis-labels)
                 (dynamic-extent labels-lengths))    

        ;; We need to "move" the drawing of the axis in the proper
        ;; position.
        ;; Probably I'll need to nudge it somewhere a bit more to properly
        ;; center the labels.

        (gp:with-graphics-translation
            (plot-frame (+ psw margin) 0)

          ;; Next we Zoom.
          
          (gp:with-graphics-scale
              (plot-frame czf 1.0)

            ;; Next we expand the 'model' (which is 'normalized'
            ;; between -1 and 1.

            (gp:with-graphics-scale
                (plot-frame (/ plot-width %width%) 1.0)

              (let ((current-tr (gp:graphics-port-transform plot-frame)))

                ;; Draw X axis labels.
                ;; (Lots of duplicated code here.  Come back and fix it.)
                        
                ;; Very hairy code below.  We cannot print out all the
                ;; labels if they overlap.
              
                (when (< plot-width labels-tot-len)
                  (format t "D-P-O X-AXIS plot width ~S to small for labels.~%"
                          plot-width)
                  )

                (when draw-labels-p
                  (loop
                     with skip = (floor max-label-len dx)
                     with print-label = 0

                     for tick-label in axis-labels
                     for i from 0
                     for tick-x = (* i dx)
                     for psw = (gp:port-string-width plot-frame
                                                     (string tick-label))
                     
                     if (zerop print-label)
                       do
                         ;; Compute the correct coordinates for the
                         ;; label.
                         (multiple-value-bind (strx stry)
                             (gp:transform-point
                              current-tr
                              (- tick-x (/ psw 2.0))
                              (+ (/ margin 2) (- %height% psh)) ; This should not change.
                              )
                            
                           ;; Now display the label without scaling.
                           ;; That is, we want it "as-is".
                           (gp:with-graphics-transform-reset (plot-frame)
                             (gp:draw-string plot-frame
                                             tick-label
                                             strx
                                             stry
                                             ))
                           )
                         (setf print-label skip)
                     else
                       do (decf print-label)
                     end))
            
                (loop for i from 0 below (length axis-labels)
                      for tick-x = (* i dx)
                      for ttick-x = (gp:transform-point
                                     current-tr
                                     tick-x
                                     (- %height% psh) ; This should not change.
                                     )
                      do
                        ;; Now display the tick without scaling.
                        ;; That is, we want it "as-is".
                        (gp:with-graphics-transform-reset (plot-frame)
                          (format t "CL-PLOT: tick at ~S ~S~%"
                                  ttick-x (- %height% psh margin))
                          (gp:draw-line plot-frame
                                        ttick-x (- %height% psh margin)
                                        ttick-x (- y tick-size) ; 0 ; (- %height% psh margin tick-size)
                                        :foreground :gray
                                        ))
                        ))
              )  ; Expand
            )  ; Zoom
          ) ; gp:with-graphics-translation
        ))))


(defmethod draw-pinboard-object ((plot-frame plot-frame)
                                 (plot-y plot-axis-y)
                                 &key
                                 (x 0)
                                 (y 0)
                                 (width 1)
                                 (height 1)
                                 )
  "Drawing the Y axis object of a 2D plot on a PLOT-FRAME.

The drawing is done by translating 'down' and by scaling by the
actual 'height' of the plot."

  (declare (ignorable x y width height)
           (type integer x y width height))

  (with-slots ((g data)
               plot-object
               draw-labels-p
               tick-size
               )
      plot-y

    (with-geometry plot-frame
      (let* ((margin 10)

             (psw (gp:port-string-width plot-frame "-00000.00"))

             (psh (gp:port-string-height plot-frame "-00000.00"))

             (plot-height (- %height% (* 2 (+ margin psh))))

             ;; This is really the PLOT-AXIS-HEIGHT
             ;; (plot-axis-height (- %height% (* 2 (+ margin psh))))
             ;; (plot-axis-height (+ psh (- %height% (* 2 (+ margin psh)))))
             ;; (plot-axis-height height)
             (plot-axis-height plot-height)

             (axis-labels (cl-plot::format-ticks plot-y))

             #|
             (n-ticks
              (if (slot-boundp plot-y 'n-ticks)
                  (plot-axis-n-ticks plot-y)
                  (1+ (length axis-labels))))
             |#

             (n-ticks (length axis-labels))

             (labels-height
              (gp:port-string-height plot-frame (first axis-labels)))
                    

             ;; (dy (/ plot-height n-ticks))
             (dy (/ %height% n-ticks))
             ;; (dy (/ (- plot-height psh) n-ticks))

             (czf (current-zoom-factor plot-frame))
             )

        (declare (type integer psw psh labels-height n-ticks margin)
                 (type real dy czf))

        ;; Debug
        #+nil
        (display-message "DPO AXIS Y~%H ~D P-H ~S"
                         height
                         plot-axis-height)

        ;; Debug
        #+nil
        (display-message "DPO AXIS Y~%Labels: ~S~%N-TICKS: ~S"
                         axis-labels
                         (slot-boundp plot-y 'n-ticks))

        ;; First we move the axis in the correct position.
        (gp:with-graphics-translation
            (plot-frame margin (+ margin psh))

          ;; Next we zoom
          (gp:with-graphics-scale
              (plot-frame 1.0 czf)

            ;; Then we rescale from [-1, 1]
            (gp:with-graphics-scale
                (plot-frame 1.0 (/ plot-axis-height %height%)) ; This is correct. We draw wrt the plot-frame.

              (let ((current-tr (gp:graphics-port-transform plot-frame)))

                ;; Draw Y axis labels.
              
                (when draw-labels-p
                  (loop
                     with skip = (floor labels-height dy)
                     with print-label = 0
                     with tick-x = (+ (- (/ margin 2)) x)
                     
                     for tick-label in (reverse axis-labels) ; Y labels must be reversed.
                     for i from 0
                     ;; for tick-y = (coerce (+ (* i dy) y) 'single-float)
                     for tick-y = (* i dy)
                     collect tick-y into ticks-y
                     if (zerop print-label)
                       do
                         (multiple-value-bind (strx stry)
                             (gp:transform-point current-tr
                                                 (- (/ margin 2)) ; Kludgy.
                                                 ;; 0
                                                 tick-y)
                           (gp:with-graphics-transform-reset (plot-frame)
                             (gp:draw-string plot-frame
                                             tick-label
                                             ;; margin
                                             strx
                                             stry
                                             )))

                         #|
                         (gp:with-graphics-transform-reset (plot-frame)
                           (gp:draw-string plot-frame
                                           tick-label
                                           ;; margin
                                           tick-x
                                           tick-y
                                           )
                           )
                         |#
                       
                         (setf print-label skip)
                     else
                       do (decf print-label)
                     end
                     finally
                       (display-message "Ys: ~S" ticks-y)
                     )
            
                  (loop for i from 0 below (length axis-labels)
                        ;; for tick-y = (+ (* i dy) y)
                        for tick-y = (* i dy)
                        do (gp:draw-line plot-frame
                                         psw tick-y
                                         (+ psw (+ 10 tick-size)) tick-y
                                         ;; :thickness 20
                                         :foreground :grey
                                         ))
              
                  ))) ; Expand
            ) ; Zoom
          ) ; gp:with-graphics-translation
        ))))


(defmethod draw-pinboard-object :before ((drawing-area plot-frame)
                                         (axis plot-axis)
                                         &key
                                         (x 0)
                                         (y 0)
                                         (width 1)
                                         (height 1)
                                         )

  (declare (ignorable x y width height)
           (type integer x y width height))

  (when *debug-draw-pb-borders*

    (gp:draw-rectangle drawing-area x y (1- width) (1- height)
                       :foreground :grey
                       :filled nil
                       :dashed t
                       :thickness (border-thickness drawing-area)))
  )


(defun tick-labels-strings (plot-labels n-ticks min max)
  (if plot-labels
      plot-labels
      (loop with delta = (/ (abs (- max min)) n-ticks)
            for i from min upto max by delta
            collect (format nil "~9,2f" i))))


;;;---------------------------------------------------------------------------
;;; draw-graph
;;;
;;; 20220426 MA: In the old, no pinboard objects implementation, these
;;; methods were needed to actually draw on the "capi device".  This
;;; is not needed now, as we draw directly on the PLOT-FRAME, which is
;;; a PINBOARD-LAYOUT.

;;; This method is probably all we need.  However, we maintain the
;;; other ones for backward compatibility.

(defmethod draw-graph ((drawing-area plot-frame)
                       (plot plot-object)
                       (d dataset)
                       &key
                       )
  (with-geometry drawing-area
    (gp:with-graphics-transform (drawing-area (plot-frame-transform drawing-area))
      (gp:with-graphics-scale (drawing-area %width% %height%)
        (draw-dataset drawing-area d)))))


(defmethod draw-graph ((drawing-area plot-frame)
                       (plot plot-object)
                       (d dataset-2d)
                       &key
                       )
  (with-geometry drawing-area
    (gp:with-graphics-transform (drawing-area (plot-frame-transform drawing-area))
      (gp:with-graphics-scale (drawing-area %width% %height%)
        (draw-dataset drawing-area d)))))


(defmethod draw-graph :before ((drawing-area plot-frame)
                               (plot plot-object)
                               (d dataset)
                               &key)
  (assert (eq d (plot-object-data plot))))


(defmethod draw-graph ((drawing-area plot-frame)
                       (plot plot-object)
                       (g graph-2d)
                       &key
                       )
  (with-geometry drawing-area
    (gp:with-graphics-transform (drawing-area (plot-frame-transform drawing-area))
      (gp:with-graphics-scale (drawing-area %width% %height%)
        (draw-dataset drawing-area g)))))


(defmethod draw-graph :before ((drawing-area plot-frame)
                               (plot plot-object)
                               (d graph)
                               &key)
  (assert (eq d (plot-object-data plot))))


;;;;---------------------------------------------------------------------------
;;;; draw-dataset

;;;; 20220426 MA:
;;;; 20221207 MA:
;;;; These methods are now called directly by the DRAW-PINBOARD-OBJECT
;;;; methods on PLOT-FRAME or PLOT-AREA and PLOT-OBJECT.
;;;;
;;:; Datasets are are drawn on PLOT-AREAs.
;;;;
;;;; 20230306 MA:
;;;; Now datasets and graphs are drawn on PLOT-FRAME.

(defmethod draw-dataset ((drawing-area plot-frame) (d dataset-2d)
                         &key
                         &allow-other-keys)
  (let* ((dsnp (dataset-normalized-points d))

         (dataset-polygons
          (if (some (lambda (xy) (eq :missing-value (second xy))) dsnp)
              (split-sequence:split-sequence-if
               (lambda (xy) (eq :missing-value (second xy)))
               dsnp)
              (list dsnp)))
         )

    ;; (format t "dataset-2d ~S~%" dsnp)
    ;; (format t "dataset-2d ~S~%" dataset-polygons)

    (dolist (dsnp dataset-polygons)
      (gp:draw-polygon drawing-area
                       dsnp
                       ;; :filled nil
                       :closed nil
                       :foreground (choose-color)))))


;;; Test placeholder

(defun point-2d-outsite-view-p (x y min-x min-y width height)
  (cond ((< x min-x) t)
        ((< (+ min-x width) x) t)
        ((< y min-y) t)
        ((< (+ min-y height) y) t)
        (t nil)))


(declaim (inline normalize-pt))

(defun normalize-pt (pt pt-min pt-max)
  (declare (type real pt pt-min pt-max))
  (/ (+ pt pt-min) pt-max)
  )

#+nil
(defun normalize-pt-unit-interval (pt pt-min pt-max)
  (declare (type real pt pt-min pt-max))
  (/ (+ pt pt-min) pt-max)
  )


(declaim (ftype (function (list) list) flatten-pair-list)
         (inline flatten-pair-list))

(defun flatten-pair-list (pair-list)
  (declare (type list pair-list))
  (loop for (x y) in pair-list
        collect x
        collect y))


(declaim (ftype (function (list) list) mirror-y-coord)
         (inline mirror-y-coord))

(defun mirror-y-coord (coord-list)
  (declare (type list coord-list))
  (loop for (x y) on coord-list by #'cddr
        collect x
        collect (- y)))


(declaim (ftype (function (sequence) list) split-polygon)
         (inline split-polygon))

(defun split-polygon (ds-polygon)
  (declare (type sequence ds-polygon))
  (if (find :missing-value ds-polygon :test #'eq :key #'second)
      (split-sequence:split-sequence :missing-value
                                     ds-polygon
                                     :test #'eq
                                     :key #'second)
      (list ds-polygon))
  )


#| Old version with PLOT-AREA
(defmethod draw-dataset ((drawing-area plot-area)
                         (g graph-2d)
                         &key
                         &allow-other-keys)

  ;; Datasets are always drawn with the correct "current zoom factor".

  (with-geometry drawing-area

    (gp:with-graphics-scale ; Scale for actual size of drawing.
        (drawing-area (/ %width% 2.0) (/ %height% 2.0))

      (gp:with-graphics-translation
          (drawing-area 1 1) ; The data points are within (-1 -1) and (1 1).

        ;; First, normalize the plot coords between -1.0 and 1.0
        ;; (making sure to break the plots when there are :missing-value's).
        ;; Next draw each dataset/polygon.

        ;; This is to check that things are ok...
        (gp:draw-line drawing-area -0.9 0.9 0.9 -0.9
                      :foreground :blue
                      :thickness 3
                      :scale-thickness nil)

        (loop for d in (graph-datasets g)
              for y from 0
              for dataset-polygon  = (dataset-normalized-points d)
              for dataset-polygons = (split-polygon dataset-polygon)
              do
                (dolist (dps dataset-polygons)
                  (format t "draw-polygon: ~S~%" dps)
                  (gp:draw-polygon drawing-area
                                   (mirror-y-coord (flatten-pair-list dps))
                                   :filled nil
                                   :closed nil
                                   ;; :foreground (choose-color i)
                                   :thickness 1 ; (/ (max %width% %height%))
                                   :scale-thickness nil
                                   )
                  )
                ) 
        ) ; with-graphics-translation
      )))
|#

;;; 2023-03-09. New version which draws on PLOT-FRAME, hence it must
;;; "reshape" down to the PLOT-OBJECT.

#|
(defmethod draw-dataset ((drawing-area plot-frame)
                         (g graph-2d)
                         &key
                         &allow-other-keys)

  ;; Datasets are always drawn with the correct "current zoom factor".

  (with-geometry drawing-area

    (gp:with-graphics-scale ; Scale for actual size of drawing.
        (drawing-area (/ %width% 2.0) (/ %height% 2.0))

      (gp:with-graphics-translation
          (drawing-area 1 1) ; The data points are within (-1 -1) and (1 1).

        ;; First, normalize the plot coords between -1.0 and 1.0
        ;; (making sure to break the plots when there are :missing-value's).
        ;; Next draw each dataset/polygon.

        ;; This is to check that things are ok...
        (gp:draw-line drawing-area -0.9 0.9 0.9 -0.9
                      :foreground :blue
                      :thickness 3
                      :scale-thickness nil)

        (loop for d in (graph-datasets g)
              for y from 0
              for dataset-polygon  = (dataset-normalized-points d)
              for dataset-polygons = (split-polygon dataset-polygon)
              do
                (dolist (dps dataset-polygons)
                  (format t "draw-polygon: ~S~%" dps)
                  (gp:draw-polygon drawing-area
                                   (mirror-y-coord (flatten-pair-list dps))
                                   :filled nil
                                   :closed nil
                                   ;; :foreground (choose-color i)
                                   :thickness 1 ; (/ (max %width% %height%))
                                   :scale-thickness nil
                                   )
                  )
                ) 
        ) ; with-graphics-translation
      )))
|#


(defmethod draw-dataset ((drawing-area plot-frame)
                         (g graph-2d)
                         &key
                         &allow-other-keys
                         &aux
                         (plot-obj
                          (first (layout-description drawing-area)))
                         )

  ;; Datasets are always drawn with the correct "current zoom factor".


  ;; 2023-03-21: I am just hacking here.  TRT will be to go back to
  ;; the three argument dispatch of DRAW-GRAPH; after all I get here
  ;; with the PLOT-OBJECT in hand from DRAW-PINBOARD-OBJECT.

  (declare (type plot-object plot-obj))

  (with-geometry drawing-area
    (multiple-value-bind (plot-obj-x
                          plot-obj-y
                          plot-obj-w
                          plot-obj-h)
        ;; (plot-object-box drawing-area)
        (plot-object-box plot-obj)

      (gp:with-graphics-post-translation
          (drawing-area plot-obj-x plot-obj-y)

        (gp:with-graphics-scale ; Scale for actual size of drawing.
            ;; (drawing-area (/ %width% 2.0) (/ %height% 2.0))
            (drawing-area (/ plot-obj-w 2.0) (/ plot-obj-h 2.0))

          (gp:with-graphics-translation
              (drawing-area 1 1) ; The data points are within (-1 -1) and (1 1).

            ;; First, normalize the plot coords between -1.0 and 1.0
            ;; (making sure to break the plots when there are :missing-value's).
            ;; Next draw each dataset/polygon.

            ;; This is to check that things are ok...
            #+nil
            (gp:draw-line drawing-area -0.9 0.9 0.9 -0.9
                          :foreground :blue
                          :thickness 3
                          :scale-thickness nil)

            (draw-zero-axes drawing-area plot-obj g)

            (loop for d in (graph-datasets g)
                  for y from 0
                  for dataset-polygon  = (dataset-normalized-points d)
                  for dataset-polygons = (split-polygon dataset-polygon)
                  do
                    (dolist (dps dataset-polygons)
                      ;; (format t "draw-polygon: ~S~%" dps)
                      (gp:draw-polygon drawing-area
                                       (mirror-y-coord (flatten-pair-list dps))
                                       :filled nil
                                       :closed nil
                                       ;; :foreground (choose-color i)
                                       :thickness 1 ; (/ (max %width% %height%))
                                       :scale-thickness nil
                                       )
                      )
                    )
            ) ; with-graphics-translation
          ) ; with-graphics-scale
        ) ; with-graphics-post-translation
      )))

;;;---------------------------------------------------------------------------
;;; Drawing the X and Y axes.
;;;
;;; 20220409: Drawing directly does not work (anymore?) on LWM. A
;;; solution is to build a "spec" (a pinboard-object) for the axes and
;;; then add them or draw them properly according to CAPI conventions.

(defun make-plot-object-axis (plot-object data coord x y width height
                                          &key
                                          (tick-size 10)
                                          (n-ticks 10)
                                          (draw-labels-p t)
                                          &allow-other-keys
                                          &aux
                                          (axis-class
                                           (ecase coord
                                             (:x 'plot-axis-x)
                                             (:y 'plot-axis-y)
                                             (:z 'plot-axis-z)
                                             ))
                                          )
  (make-instance axis-class
                 :data data
                 :coord coord ; Redundant
                 :x x
                 :y y
                 :height height
                 :width width
                 :plot-object plot-object
                 :draw-labels-p draw-labels-p
                 :tick-size tick-size
                 :n-ticks n-ticks
                 ))


(defun draw-zero-axes (drawing-area plot data)
  "Draws axes centered on the 'zero' of the dataset."
  ;; Eventually the "zero" will be selectable by the user.
  ;; Note that this function transforms the graph or dataset zero to
  ;; the [-1, 1] interval and then draws it, assuming all the proper
  ;; transforms are in place.

  (declare (type plot-frame drawing-area)
           (type plot-object plot)
           (ignorable drawing-area plot data)
           )

  (flet ((normalize-zero (min max)
           (declare (type real min max))
           (cond ((<= min 0.0 max)
                  (plambdaot::normalize-pt-unit-interval 0.0 min max))
                 ((< 0.0 min) -1.0)
                 ((> 0.0 max) 1.0))
           )
         )
    (let* ((x-min (graph-min data :x))
           (y-min (graph-min data :y))
           (x-max (graph-max data :x))
           (y-max (graph-max data :y))
           (zero-x-tr (normalize-zero x-min x-max))
           (zero-y-tr (normalize-zero y-min y-max))
           )

      (declare (type real x-min x-max y-min y-max zero-x-tr zero-y-tr))

      (gp:draw-line drawing-area zero-x-tr 1.0 zero-x-tr -1.0
                    :scale-thickness nil
                    :thickness 3
                    :foreground :orange)

      (gp:draw-line drawing-area -1.0 zero-y-tr 1.0 zero-y-tr
                    :scale-thickness nil
                    :thickness 3
                    :foreground :orange)
                    
      )))


;;;---------------------------------------------------------------------------
;;; Legend

(defclass plot-frame-object-legend (drawn-pinboard-object)
  ((legend-items :accessor legend-items :initarg :legend-items)))


(defparameter *legend-horizontal-margin* 4)
(defparameter *legend-vertical-margin* 4)


(defmethod compute-legend-size ((drawing-area plot-frame) (d dataset))
  (values (* 2 *legend-horizontal-margin*)
          (* 2 *legend-vertical-margin*))
  )


(defmethod compute-legend-size ((drawing-area plot-frame) (d graph))
  (let* ((ds (graph-datasets d))
         (psh (gp:port-string-height drawing-area "FOO"))
         (max-label-width
          (loop for d in ds
                maximize (gp:port-string-width drawing-area (string (name d)))))
         (max-labels-height (* (+ psh *legend-vertical-margin*) (length ds)))
         (legend-width (+ max-label-width (* *legend-horizontal-margin* 2)))
         (legend-height (+ max-labels-height (* *legend-vertical-margin* 2)))
         )
    (values legend-width
            legend-height)
    ))


(defmethod draw-legend ((drawing-area plot-frame)
                        (plot plot-object)
                        (d dataset)
                        &key)

  (let ((x 1.2)
        (y 0)
        )
    (gp:draw-string drawing-area (string (dataset-name d)) x y
                    :foreground (choose-color))
    ))


(defmethod draw-legend ((drawing-area plot-frame)
                        (plot plot-object)
                        (g graph)
                        &key 
                        )
  (with-geometry plot
    (let* ((ds (graph-datasets g))
           (psh (gp:port-string-height drawing-area "FOO"))
           (max-label-width
            (loop for d in ds
                  maximize (gp:port-string-width drawing-area (string (name d)))))
           (max-labels-height (* (+ psh *legend-vertical-margin*) (length ds)))
           (legend-width (+ max-label-width (* *legend-horizontal-margin* 2)))
           (legend-height (+ max-labels-height (* *legend-vertical-margin* 2)))
           (legend-x (- %width% (+ legend-width *legend-horizontal-margin*)))
           (legend-y *legend-vertical-margin*)
           )

      (gp:draw-rectangle drawing-area
                         legend-x legend-y
                         legend-width legend-height
                         :foreground :white
                         :filled t)
      (gp:draw-rectangle drawing-area
                         legend-x legend-y
                         legend-width legend-height)

      (loop with x = (+ legend-x *legend-horizontal-margin*)
            for d in ds
            for y from (+ legend-y *legend-horizontal-margin* psh)
              by (+ *legend-horizontal-margin* psh)
            for i from 0
            do (gp:draw-string drawing-area (string (name d))
                               x y
                               :foreground (choose-color i))
            ))
      ))


;;; (defgeneric add-plot (new-plot-designator &optional current-plot-output &key &allow-other-keys))

;;; (defgeneric remove-plot (plot-designator &optional current-plot-output &key &allow-other-keys))

;;; (defgeneric clear (&optional current-plot-output))


;;;---------------------------------------------------------------------------
;;; Interface functions.

(defun resize-capi-device (capi-device x y w h)
  (erase-plot-device capi-device)
  (dolist (plot-frame (layout-description capi-device))
    (resize-plot-device-object capi-device plot-frame x y w h)
    )
  ;; (erase-plot-device capi-device)
  )


(defun plot-interface-plot-frame (capi-plot-interface &optional (plot-frame-n 0))
  (declare (type plot-interface capi-plot-interface)
           (type fixnum plot-frame-n)
           (ignore plot-frame-n)
           )

  ;; Badly hardcoded!!!

  (first (layout-description (plot-interface-device capi-plot-interface)))
  )


(defun plot-interface-plot-frame-elements (capi-plot-interface
                                           &optional (plot-frame-n 0))
  (let ((frame
         (plot-interface-plot-frame capi-plot-interface plot-frame-n))
        )
    (apply #'values (layout-description frame))))


;;;; end of file -- plot-protocol-impl-capi.lisp --
