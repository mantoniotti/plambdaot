;;; -*- Mode: Lisp -*-

;;;; lw-plot-interface.lisp
;;;;
;;;; Plotting utilities for LW; LW plot interface.
;;;;
;;;; See the file COPYING for copyright and licensing information.
;;;;
;;;; TODO:
;;;;
;;;; 20221204 MA: Fix images for toolbar.


(in-package "CL-PLOT-CAPI")

#+not-working ; Fix this later.
(eval-when (:load-toplevel :execute :compile-toplevel)
  (let ((images-directory (lispworks:current-pathname
                           (make-pathname :directory '(:relative :up "images")
                                          :name nil
                                          :type nil)))
        )
    (gp:register-image-translation
     'toolbar-zoomplus-icon
     (gp:read-external-image (make-pathname :name "zoomplus2" :type "bmp"
                                            :defaults images-directory)
                             :transparent-color-index 1))

    (gp:register-image-translation
     'toolbar-zoomminus-icon
     (gp:read-external-image (make-pathname :name "zoomminus2" :type "bmp"
                                            :defaults images-directory)
                             :transparent-color-index 1))
    ))



(gp:register-image-translation
 'toolbar-zoomplus-icon
 #.(gp:read-external-image (lispworks:current-pathname
                            (make-pathname :directory '(:relative :up "images")
                                           :name "zoomplus2"
                                           :type "bmp"
                                           ))
                           :type :bmp
                           ;; :transparent-color-index 7
                           ))

(gp:register-image-translation
 'toolbar-zoomminus-icon
 #.(gp:read-external-image (lispworks:current-pathname
                            (make-pathname  :directory '(:relative :up "images")
                                            :name "zoomminus2"
                                            :type "bmp"))
                           :type :bmp
                           ;; :transparent-color-index 7
                           ))


;;;---------------------------------------------------------------------------
;;; plot-interface

(define-interface plot-interface ()
  ((start-x :accessor start-x :initform 0)
   (start-y :accessor start-y :initform 0)
   (viewport-x :accessor viewport-x :initform 0)
   (viewport-y :accessor viewport-y :initform 0)

   (button-pressed-p :accessor button-pressed-p :initform nil)

   )

  (:panes
   ; #+win32
   (plot-toolbar
    toolbar
    :items (list (make-instance
                  'toolbar-component
                  :items (list :std-file-new :std-file-open))

                 (make-instance
                  'toolbar-component
                  :items (list (make-instance
                                'toolbar-button
                                :tooltip "Zoom in"
                                :image 'toolbar-zoomplus-icon
                                :remapped 'zoom-in-menu-item)
                               (make-instance
                                'toolbar-button
                                :tooltip "Zoom out"
                                :image 'toolbar-zoomminus-icon
                                :remapped 'zoom-out-menu-item)))
                 )
    :max-height t
    :max-width t))

  (:layouts
   (plot-column-layout
    column-layout
    '(
      ; #+win32
      plot-toolbar
      plot-device
      )
    :gap 0
    )

   (plot-device
    capi-device ; Essentially a PINBOARD-LAYOUT FTTB.
    ()
    :reader plot-interface-device
    :resize-callback 'resize-capi-device
    :visible-min-width 200
    :visible-min-height 200
    :internal-min-width 200
    :internal-min-height 200
    :background :grey95
    ;; :background :cyan
    ))
  
  (:menu-bar
   file-menu
   edit-menu
   view-menu
   insert-menu
   tools-menu
   windows-menu
   help-menu)
  
  (:menus
   (help-menu
    "Help"
    ((:component ("Plotting Help"))
     (:component ("About CL-PLOT"))))

   (windows-menu
    "Windows"
    ())

   (tools-menu
    "Tools"
    ((:component (("Edit Plot" :enabled-slot nil)
                  ("Zoom In" :name 'zoom-in-menu-item
                             :callback 'plot-interface-zoom-in
                             :callback-type :interface)
                  ("Zoom Out" :name 'zoom-out-menu-item
                              :callback 'plot-interface-zoom-out
                              :callback-type :interface)
                  ))
     (:component ("Data Statistics"))))

   (insert-menu
    "Insert"
    ((:component ("X Label"
                  "Y Label"
                  "Z Label"
                  "Title"))
     (:component ("Legend"))))

   (view-menu
    "View"
    ())

   (edit-menu
    "Edit"
    ((:component ("Undo"))
     (:component ("Cut"
                  "Copy"
                  "Paste"
                  "Clear"))
     (:component ("Select All"))))

   (file-menu
    "File"
    ((:component ("New"
                  "Open ..."
                  "Save"
                  "Save as ..."))
     (:component ("Print"))
     (:component ("Close")))))

  (:default-initargs
   :best-height 30
   :best-width 450
   :layout 'plot-column-layout
   :window-styles '(:internal-borderless) ; Finally found!
   :title "CL Plot (CAPI)"
   ;; :background :red
   )
  )


(defun new-plot-interface (&key show (set-as-output t))
  (let ((npi (make-instance 'plot-interface)))
    (when set-as-output
      (setf cl-plot::*standard-plot-device* npi))
    (if show
        (capi:display npi)
        npi)))


;;;; end of file -- lw-plot-interface.lisp
