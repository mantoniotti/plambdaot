;;;; -*- Mode: Lisp -*-

;;;; drawing.lisp --
;;;; Drawing utilities
;;;;
;;;; See the file COPYING in top folder for copyright and licensing information.
;;;;
;;;; Notes:
;;;;
;;;; 20221130 MA: created.


(in-package "CL-PLOT-CAPI")

;;;; draw-unit-grid

(defun draw-unit-grid (drawing-area)
  (declare (type plot-area drawing-area))

  (with-geometry drawing-area
    (let ((w/2 (round (/ %width% 2.0)))
          (h/2 (round (/ %height% 2.0)))
          )

      (gp:draw-line drawing-area w/2 0 w/2 %height% :dashed t)
      (gp:draw-line drawing-area 0 h/2 %width% h/2 :dashed t)
      ))
  )
  

;;;; end of file -- drawing.lisp --
