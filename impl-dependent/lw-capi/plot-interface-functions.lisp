;;; -*- Mode: Lisp -*-

;;;; plot-interface-functions.lisp
;;;;
;;;; Plotting utilities for LW; functions for LW interface.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(in-package "CL-PLOT-CAPI")

(defun plot-interface-zoom-in (interface)
  (declare (type plot-interface interface))
  (let ((plot-frame
         (capi-device-plot-frame (plot-interface-device interface))))
    (declare (type (or null plot-frame) plot-frame))
    (when plot-frame
      (setf (current-operation plot-frame)
            :zoom

            (simple-pane-cursor plot-frame)
            :crosshair

            
            (previous-zoom-factor plot-frame)
            (current-zoom-factor plot-frame)
            )
      (incf (current-zoom-factor plot-frame)
            (zoom-delta plot-frame))

      (with-geometry
          plot-frame
        (resize-plot-device-object (plot-frame-device plot-frame)
                                   plot-frame
                                   nil
                                   nil
                                   %width%
                                   %height%))
      )))


(defun plot-interface-zoom-out (interface)
  (declare (type plot-interface interface))
  (let ((plot-frame
         (capi-device-plot-frame (plot-interface-device interface))))
    (declare (type (or null plot-frame) plot-frame))
    (when plot-frame
      (setf (current-operation plot-frame)
            :zoom
            
            (simple-pane-cursor plot-frame)
            :crosshair

            (previous-zoom-factor plot-frame)
            (current-zoom-factor plot-frame)
            )
      (decf (current-zoom-factor plot-frame)
            (zoom-delta plot-frame))
      (with-geometry
          plot-frame
        (resize-plot-device-object (plot-frame-device plot-frame)
                                   plot-frame
                                   nil
                                   nil
                                   %width%
                                   %height%))
      )))

;;; end of file -- plot-interface-functions.lisp --
