;;;; -*- Mode: Lisp -*-

;;;; plot-setup.lisp
;;;; Cleaned up version of CAPI plot protocol implementation.
;;;;
;;;; See the file COPYING in top folder for copyright and licensing information.


(in-package "CL-PLOT-CAPI")

;;; Notes:
;;;
;;; Each plot is displayed on a pinboad-layout.  The actual widget
;;; hierarchy is going to be the following:
;;;
;;; capi-device (a pinboard-layout)
;;;  +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-object (a pinboard-object, i.e. the actual plot)
;;;       +---> plot-x-axis (a pinboard-object)
;;;       +---> plot-y-axis (a pinboard-object)
;;;
;;; 20230201 MA:
;;; It was:
;;;
;;; capi-device (a pinboard-layout)
;;; +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-area (a simple-pinboard-layout)
;;;             +---> plot-object (a pinboard-object, i.e. the actual plot)
;;;       +---> plot-x-axis (a pinboard-object)
;;;       +---> plot-y-axis (a pinboard-object)


;;; plot-setup

(defgeneric plot-setup (target &rest keys &key &allow-other-keys))


(defmethod plot-setup ((interface plot-interface)
                       &rest
                       keys
                       &key
                       &allow-other-keys)
  (apply #'execute-with-interface interface
         #'plot-setup
         (plot-interface-device interface)
         keys))


(defmethod plot-setup ((device capi-device)
                       &rest
                       keys
                       &key
                       ((:graph-2d g))

                       (draw-legend-p t)
                       (draw-x-labels-p t)
                       (draw-y-labels-p t)
                       (tick-size 10)
                       (margin 10)
                       (spacing 2)
                       &allow-other-keys)

  (declare (ignorable spacing))

  (multiple-value-bind (min-width min-height)
      (get-constraints device) ; (values 10 10)

    (assert (and min-width min-height) (min-width min-height))

    (with-geometry device
      (let* ((w (or %width% min-width))
             (h (or %height% min-height))
             (plot-frame-w (round (* w 0.8)))
             (plot-frame-h (round (* h 0.8)))
             (plot-frame-x (round (/ (- w plot-frame-w) 2.0)))
             (plot-frame-y (round (/ (- h plot-frame-h) 2.0)))

             (psw (gp:port-string-width device "-00000.00"))
             (psh (gp:port-string-height device "-00000.00"))

             (plot-object-w (- plot-frame-w (* 2 (+ margin psw))))
             (plot-object-h (- plot-frame-h (* 2 (+ margin psh))))
             (plot-object-x (+ margin psw))
             (plot-object-y (+ margin psh))

             (plot-x-axis-x plot-object-x)
             (plot-x-axis-y (+ plot-object-y plot-object-h))
             (plot-x-axis-w plot-object-w)
             (plot-x-axis-h (+ margin psh))

             (plot-y-axis-x margin)
             ;; (plot-y-axis-y margin)
             (plot-y-axis-y plot-object-y)
             ;; (plot-y-axis-w (+ margin psw))
             (plot-y-axis-w psw) ; This is not enough!  Come back to
                                 ; fix these dimensions.
             (plot-y-axis-h plot-object-h)
             ;; (plot-y-axis-h (+ plot-object-h psh))

             (new-plot-frame
              (make-instance 'plot-frame
                             :x plot-frame-x
                             :y plot-frame-y
                             :width plot-frame-w
                             :height plot-frame-h

                             :visible-min-width  plot-frame-w
                             :visible-min-height plot-frame-h
                             :visible-max-width  plot-frame-w
                             :visible-max-height plot-frame-h

                             :border-width 4

                             :draw-with-buffer t ; Windows may need it.

                             :background :white
                             :input-model '(((:button-1 :press) press-button)
                                            ((:button-1 :release) release-button)
                                            )
                             ))
             
             (new-plot-object
              (make-instance 'plot-object

                             :x plot-object-x
                             :y plot-object-y
                             :width  plot-object-w
                             :height plot-object-h
                             :internal-min-width  plot-object-w
                             :internal-min-height plot-object-h
                             :internal-max-width  plot-object-w
                             :internal-max-height plot-object-h

                             :draw-legend-p   draw-legend-p
                             :draw-x-labels-p draw-x-labels-p
                             :draw-y-labels-p draw-y-labels-p
                             :tick-size tick-size
                             ;; :data g
                             ))

             (plot-object-title
              (when g
                (make-plot-object-title device g
                                        plot-frame-x
                                        plot-frame-y
                                        (1- plot-frame-w)
                                        (1- plot-frame-h))))

             (x-axis-object
              (when g
                (make-plot-object-axis new-plot-object
                                       g
                                       :x
                                       plot-x-axis-x
                                       plot-x-axis-y
                                       plot-x-axis-w
                                       plot-x-axis-h
                                       :draw-labels-p draw-x-labels-p
                                       :tick-size tick-size)))

             (y-axis-object
              (when g
                (make-plot-object-axis new-plot-object
                                       g
                                       :y
                                       plot-y-axis-x
                                       plot-y-axis-y
                                       plot-y-axis-w
                                       plot-y-axis-h
                                       :draw-labels-p draw-y-labels-p
                                       :tick-size tick-size)))
             )


        ;; Build the hierarchy.

        (setf (layout-description device) (list new-plot-frame))

        (manipulate-pinboard new-plot-frame new-plot-object :add-top)

        (when g
          (manipulate-pinboard new-plot-frame x-axis-object :add-top)
          (manipulate-pinboard new-plot-frame y-axis-object :add-top))

        (when g
          (manipulate-pinboard device plot-object-title :add-top)) 

        ;; Return the (modified) CAPI-DEVICE and the objects in the
        ;; hierarchy.

        (values device
                new-plot-frame
                new-plot-object
                x-axis-object
                y-axis-object)
        ))))

;;;; end of file -- plot-setup.lisp --
