;;; -*- Mode: Lisp -*-

;;;; plot-object.lisp
;;;;
;;;; Plotting utilities for LW; CAPI devices and pinboard objects.
;;;;
;;;; See the file COPYING for copyright and licensing information.


(in-package "CL-PLOT-CAPI")

;;; Note:
;;; Each plot is displayed on a pinboad-layout.  The actual widget
;;; hierarchy is going to be the following:
;;;
;;; capi-device (a pinboard-layout)
;;;  +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-object (a pinboard-object, i.e. the actual plot)
;;;
;;; It was:
;;;
;;; capi-device (a pinboard-layout)
;;; +---> plot-frame (a pinboard-layout, what Matlab calls AXES)
;;;       +---> plot-area (a simple-pinboard-layout)
;;;             +---> plot-object (a pinboard-object, i.e. the actual plot)


;;;---------------------------------------------------------------------------
;;; Definitions.


;;; plot-object --

(defclass plot-object (plot-element) ; No need to be a DRAWN-PINBOARD-OBJECT.
  ((data :accessor plot-object-data
         :initarg :data
         :initform nil
         :type (or null graph dataset))
   (draw-legend-p :accessor draw-legend-p :initarg :draw-legend-p)
   (draw-x-labels-p :accessor draw-x-labels-p
                  :initarg :draw-x-labels-p)
   (draw-y-labels-p :accessor draw-y-labels-p
                    :initarg :draw-y-labels-p)
   (tick-size :accessor tick-size
              :initarg :tick-size)
   )
  (:default-initargs
   :draw-legend-p t
   :draw-x-labels-p t
   :draw-y-labels-p t
   :tick-size 10
   )
  (:documentation "The Plot Object Class.")
  )


(defgeneric plot-object-p (x)
  (:method ((x plot-object)) t)
  (:method ((x t)) nil))


;;;---------------------------------------------------------------------------
;;; Implementation (to be put elsewhere)

(defun plot-frame-device (plot-frame)
  (declare (type plot-frame plot-frame))
  (element-parent plot-frame))

(defun plot-object-frame (plot-object)
  (declare (type plot-object plot-object))
  (element-parent plot-object))

(defun plot-object-device (plot-object)
  (declare (type plot-object plot-object))
  (element-parent (element-parent plot-object)))

(defun capi-device-plot-frame (capi-device)
  (declare (type capi-device capi-device))
  (find-if #'plot-frame-p (layout-description capi-device)))


;;; plot-object-box

(declaim (ftype (function (plot-frame)
                          (values integer integer integer integer))
                plot-object-box)

         (inline plot-object-box))

(defun plot-object-box (plot-object)
  "Returns the X, Y, W and H of a PLOT-OBJECT."

  (declare (type plot-object plot-object))

  ;; The function is just a wrapper around
  ;; capi:static-layout-child-geometry, but it is here for (semi) back
  ;; compatability.

  (static-layout-child-geometry plot-object)
  )


#|
(defun plot-object-box (drawing-area &aux (margin 10))
  "Returns the X, Y, W and H of a plot object within DRAWING-AREA.

The function returns four values. DRAWING-AREA is a PLOT-FRAME.  The
values returned are computed w.r.t., the PLOT-FRAME on DRAWING-AREA."

  (declare (type plot-frame drawing-area))

  (multiple-value-bind (min-width min-height)
      (get-constraints drawing-area) ; (values 10 10)

    (assert (and min-width min-height) (min-width min-height))
    
    (with-geometry drawing-area

      (let ((psw (gp:port-string-width drawing-area "-00000.00"))
            (psh (gp:port-string-height drawing-area "-00000.00"))
            )

        (let ((plot-object-w (round (- %width% (* 2 (+ margin psw)))))
              (plot-object-h (round (- %height% (* 2 (+ margin psh)))))
              ;; (plot-object-x (round (+ %x% margin psw)))
              ;; (plot-object-y (round (+ %y% margin psh)))
              (plot-object-x (round (+ margin psw)))
              (plot-object-y (round (+ margin psh)))
              )

          (multiple-value-bind (xx yy ww hh)
              (static-layout-child-geometry (first (layout-description drawing-area)))

            #+nil
            (display-message "P-O-B~@
                            X ~S Y ~S W ~S H ~S~@
                            X ~S Y ~S W ~S H ~S~@
                            XX ~S YY ~S WW ~S HH ~S"
                             %x% %y% %width% %height%
                             plot-object-x plot-object-y plot-object-w plot-object-h
                             xx yy ww hh
                             ))

          (values plot-object-x plot-object-y
                  plot-object-w plot-object-h)
          )))
    )
  )
|#

;;; end of file -- plot-object.lisp --
