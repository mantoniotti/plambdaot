;;; -*- Mode: Lisp -*-

(in-package "CL-USER")


(defvar sin-interface nil)

(defvar sin-data nil)

(defvar sin-broken-data nil)

(defvar sin-graph nil)


(defvar cos-data nil)

(defvar cos-broken-data nil)

(defvar cos-graph nil)


(defun test-sin-data ()
  (let ((y-data (loop for x from (- pi) to (* 2 pi) by 0.1 collect (sin x)))
        (x-data (loop for x from (- pi) to (* 2 pi) by 0.1 collect x))
        (y-data-broken
         (loop for x from (- pi) to (* 2 pi) by 0.1
               for cos-x = (cos x)
               if (<= cos-x 0.5)
                 collect cos-x
               else
                 collect :break))
        )
    (declare (ignore y-data-broken))

    (setf sin-data (cl-plot:make-dataset :y-data y-data :x-data x-data)
          sin-graph (make-instance 'cl-plot:graph-2d))

    (cl-plot:add-dataset sin-graph sin-data)))


(defun test-cos-data ()
  (let ((y-data (loop for x from (- pi) to (* 2 pi) by 0.1 collect (cos x)))
        (x-data (loop for x from (- pi) to (* 2 pi) by 0.1 collect x))
        (y-data-broken
         (loop for x from (- pi) to (* 2 pi) by 0.1
               for sin-x = (sin x)
               if (<= sin-x 0.5)
                 collect sin-x
               else
                 collect :break))
        )
    (declare (ignore y-data-broken))

    (setf cos-data (cl-plot:make-dataset :y-data y-data :x-data x-data)
          cos-graph (make-instance 'cl-plot:graph-2d))

    (cl-plot:add-dataset cos-graph cos-data)))

;;; end of file -- test.lisp --
