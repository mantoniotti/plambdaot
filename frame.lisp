;;;; -*- Mode: Lisp -*-

;;;; frame.lisp
;;;;
;;;; Plotting utilities for Common Lisp; "frame" definitions.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(in-package "CL-PLOT")


;;; frame --
;;;
;;; A frame contains a graph, which is essentially a set of datasets,
;;; and a set of axis.  Alongside a style object.

(defclass frame ()
   ((graph :accessor frame-graph
           :initarg :graph)

    (axes :accessor frame-axes
          :initarg :axes
          :initform ())

    (style :accessor frame-style
           :initarg :style
           :initform (make-instance 'style)
           )
    )
   )


(defgeneric frame-p (x)
  (:method ((x frame)) t)
  (:method ((x t)) nil))

(defun is-frame (x) (frame-p x))


;;; cartesian-frame --
;;;
;;; The most common kind of frames.  It has axes X, Y and, possibly, Z.

(defclass cartesian-frame (frame)
  ()
  )


(defgeneric cartesian-frame-p (x)
  (:method ((x cartesian-frame)) t)
  (:method ((x t)) nil))

(defun is-cartesian-frame (x) (cartesian-frame-p x))


;;; Frame protocol.
;;; ---------------

(defgeneric frame-x-axis (f)
  (:method ((f cartesian-frame)) (first (frame-axes f)))
  )


(defgeneric frame-y-axis (f)
  (:method ((f cartesian-frame)) (second (frame-axes f)))
  )


(defgeneric frame-z-axis (f)
  (:method ((f cartesian-frame)) (third (frame-axes f)))
  )


;;; Plot Component Protocol implementation.

(defmethod part-of ((f frame)) nil)

(defmethod parent ((f frame)) nil)

(defmethod represents ((f frame)) nil)

(defmethod representation ((f frame)) nil)

(defmethod components ((f frame))
  (list* (frame-graph f) (frame-axes f)))

;;;; end of file -- frame.lisp --
