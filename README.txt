CL-PLOT
A library for plotting on different CL platforms.
=================================================

Currently supported:
* Lispworks:  Windows, OS X

Under construction:
* OpenGL


The library is organized around the following concepts: plots,
devices, and datasets.  The way to think about these is the following:
a plot contains several (possibly related) datasets viewed as graphs.
A plot is "displayed" on a device.

Datasets
--------

Datasets can be 2D or 3D.  Each dataset contains data points that have
the appropriate format.


Graphs
------

A graph is a container for the datasets and encapsulates the way a
given set of datasets can be displayed.


Devices
-------

A device is the "output sink" of the plotting commands.  Typically it
is a window in a given gui.
