# PLAMBDAOT: A library for plotting on different CL platforms.

See the file COPYING for copyright and licensing information.

Ok.  The title is a tad overselling the beast (just a tad...)

Currently supported:
* Lispworks:  Windows, OS X, may work on Linux/Gtk

Under construction:
* CLIM, OpenGL


The library is organized around the following concepts: plots,
devices, and datasets.  The way to think about these is the following:
a plot contains several (possibly related) datasets viewed as graphs.
A plot is "displayed" on a device.


## Datasets

Datasets can be 2D or 3D (not yet).  Each dataset contains data points
that have the appropriate format.


## Graphs

A graph is a container for the datasets and encapsulates the way a
given set of datasets can be displayed.


## Frames

A frame is the container for a graph (what Matlab calls "Axes").


## Axis

A representation of an axis in a frame.


## Style

A representation of the visual properties of each of the elements
above.  Each Frame has a style and each of its elements may use it or
get one of its own.


## Devices

A device is the "output sink" of the plotting commands.  Typically it
is a window in a given gui.


Enjoy.
