;;;; -*- Mode: Lisp -*-

;;;; style.lisp
;;;;
;;;; Plotting utilities for Common Lisp; "style" definitions.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(in-package "CL-PLOT")


;;; style --
;;;
;;; A style contains the visual characteristics of a component and,
;;; possiby, of its subcomponents.

(defclass style ()
  ((foreground :accessor style-foreground
               :initarg :foreground
               :initform :default)

   (background :accessor style-background
               :initarg :background
               :initform :default)

   (thickness :accessor style-thickness
               :initarg :thickness
               :initform :default)

   )
  )


(defgeneric style-p (x)
  (:method ((x style)) t)
  (:method ((x t)) nil))

(defun is-style (x) (style-p x))


;;; Style protocol.
;;; ---------------


;;;; end of file -- style.lisp --
