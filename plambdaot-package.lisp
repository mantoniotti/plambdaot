;;; -*- Mode: Lisp -*-

;;;; plambdaot-package.lisp
;;;; 
;;;; Plotting utilities (mostly for LW).
;;;;
;;;; See the file COPYING for copyright and licensing information.

(defpackage "IT.UNIMIB.DISCO.MA.PLAMBDAOT" (:use "COMMON-LISP")
  (:nicknames "PLAMBDAOT" "CL-PLOT" "LW-PLOT") ; Backward compatibility.
  (:documentation "The PLAMBDAOT Package.

This is the top package of the system.")

  (:export
   "PLOT"
   "PLOT-DATA"
   )

  (:export
   "DEVICE"
   "DEVICE-P"
   "IS-DEVICE"

   "ADD-DEVICE"
   "REMOVE-DEVICE"

   "CURRENT-DEVICE"
   "LIST-DEVICES"
   )

  (:export
   "DATASET"
   "DATASET-2D"
   "DATASET-3D"

   "DATASETP"
   "DATASET-P"
   "IS-DATASET"

   "MAKE-DATASET"

   "DATASET-NAME"

   "DATASET-X-DATA"
   "DATASET-Y-DATA"
   "DATASET-Z-DATA"

   "DATASET-X-MAX"
   "DATASET-Y-MAX"
   "DATASET-Z-MAX"

   "DATASET-X-MIN"
   "DATASET-Y-MIN"
   "DATASET-Z-MIN"

   "DATASET-NORMALIZED-POINTS"
   )

  (:export
   "BAR-DATASET")


 (:export
   "PLOT-COMPONENT"
   "PLOT-COMPONENT-P"
   "IS-PLOT-COMPONENT"

   "NAME-OF"
   "PART-OF"
   "PARENT"

   "REPRESENTS"
   "REPRESENTATION"
   )


  (:export
   "GRAPH"
   "CARTESIAN-GRAPH"

   "GRAPH-2D"
   "GRAPH-3D"

   "GRAPHP"

   "MAKE-GRAPH"

   "GRAPH-NAME"

   "GRAPH-DATASETS"

   "GRAPH-X-DATA"
   "GRAPH-Y-DATA"
   "GRAPH-Z-DATA"

   "GRAPH-MAX"
   "GRAPH-X-MAX"
   "GRAPH-Y-MAX"
   "GRAPH-Z-MAX"

   "GRAPH-MIN"
   "GRAPH-X-MIN"
   "GRAPH-Y-MIN"
   "GRAPH-Z-MIN"

   "ADD-DATASET"
   "DELETE-DATASET"

   "X-LABELS"
   "Y-LABELS"
   "Z-LABELS"
   )

  (:export
   "AXIS"
   "AXIS-P"
   "IS-AXIS"

   "AXIS-KIND"
   "CARTESIAN-AXES"
   )

  (:export
   "FRAME"
   "FRAME-P"
   "IS-FRAME"
   )

  (:export
   "STYLE"
   "STYLE-P"
   "IS-STYLE"
   )
  
  )

;;; end of file -- plambdaot-package.lisp --
