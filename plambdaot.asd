;;;; -*- Mode: Lisp -*-

;;;; plambdaot.asd
;;;; 
;;;; Plotting utilities (mostly for LW).
;;;;
;;;; See the file COPYING for copyright and licensing information.

(asdf:defsystem "plambdaot"
  :author "Marco Antoniotti <mantoniotti@common-lisp.net>"
  :licence "BSD"

  :description "A Common Lisp plotting library (mostly LW CAPI FTTB)."

  :serial t
  :components ((:file "plambdaot-package")
               (:file "type-declarations")
               (:file "utilities")
               (:file "math-utilities")

               (:file "plot-component")

               (:file "dataset")
               (:file "dataset-implementation")

               (:file "graph")
               (:file "graph-implementation")

               (:file "axis")
               (:file "axis-implementation")

               (:file "style")

               (:file "frame")

               (:file "device")

               (:file "plot-protocol")

               (:file "points")

               (:module "impl-dependent"
                :serial t
                :components (
                             #+lispworks
                             (:module "lw-capi"
                              :serial t
                              :components ((:file "plambdaot-capi-pkg")
                                           ;; (:file "cl-plot-capi-pkg")
                                           (:file "logging")
                                           (:file "canonical-transforms")

                                           (:file "capi-device")

                                           (:file "plot-frame")
                                           (:file "plot-area")
                                           (:file "plot-element")
                                           (:file "plot-axes")
                                           (:file "plot-object")
                                           (:file "bordered-output-pane")

                                           (:file "lw-plot-interface")
					   (:file "plot-protocol-impl-capi")
                                           (:file "plot-setup")
                                           (:file "gestures")
                                           (:file "drawing")
                                           (:file "plot-interface-functions")
                                           ;; (:file "test")
                                           ))
                             )))
  :depends-on ("split-sequence"
               "clad"
               ;; "letv"
               )
  )

;;;; end of file -- plambdaot.asd
