;;;; -*- Mode: Lisp -*-

(in-package "CL-PLOT")

;;;; dataset.lisp
;;;;
;;;; Plotting utilities for Common Lisp; definition of a "dataset".
;;;;
;;;; See the file COPYING for copyright and licensing information.


;;; Values
;;; ------

(defconstant +missing+ :missing) ; Should work also on fascist SBCL.

(deftype missing ()
  '(eql :missing))


(deftype dataset-number-value ()
  '(or number missing))


(defun missingp (x)
  (eq :missing x))


(defun is-missing (x)
  (missingp x))


;;; dataset
;;; -------

(defclass dataset ()
  ((name :accessor dataset-name :initarg :name)
   
   (raw-data :accessor dataset-raw-data :initarg :raw-data)
   (normalized-points :accessor dataset-normalized-points :initform ())

   ;; Plot component protocol support
   (in-graph :accessor in-graph :initarg :in-graph)
   )

  (:default-initargs
   :name (gensym "DATASET-")
   :raw-data '()
   :in-graph nil)

  (:documentation "The Dataset Class.")
  )

(defgeneric datasetp (x)
  (:method ((x dataset)) t)
  (:method ((x t)) nil))


(defun dataset-p (x) (datasetp x))

(defun is-dataset (x) (datasetp x))


;;; dataset-nd --

(defclass dataset-nd (dataset)
  ()
  )

(defgeneric dataset-nd-p (x)
  (:method ((x dataset-nd)) t)
  (:method ((x t)) nil))


;;; dataset-2d --

(defclass dataset-2d (dataset-nd)
  ((x-data :accessor dataset-x-data :initarg :x-data)
   (x-max :reader dataset-x-max)
   (x-min :reader dataset-x-min)

   (y-data :accessor dataset-y-data :initarg :y-data)
   (y-max :reader dataset-y-max)
   (y-min :reader dataset-y-min)
   )
  (:documentation "The 2D Dataset Class.")
  (:default-initargs  :x-data '() :y-data '())
  )

(defgeneric dataset-2d-p (x)
  (:method ((x dataset-2d)) t)
  (:method ((x t)) nil))


;;; dataset-3d --

(defclass dataset-3d (dataset-2d)
  ((z-data :accessor dataset-z-data :initarg :z-data)
   (z-min :accessor dataset-z-min)
   (z-max :accessor dataset-z-max)
   )
  (:documentation "The 3D Dataset Class.")
  (:default-initargs :z-data '())
  )

(defgeneric dataset-3d-p (x)
  (:method ((x dataset-3d)) t)
  (:method ((x t)) nil))


;;; group-dataset --

(defclass group-dataset (dataset-nd)
  ((x-data :accessor x-data :initarg :x-data)))

(defgeneric group-dataset-p (x)
  (:method ((x group-dataset)) t)
  (:method ((x t)) nil))


;;; bar-dataset --

(defclass bar-dataset (group-dataset) ())

(defgeneric bar-dataset-p (x)
  (:method ((x bar-dataset)) t)
  (:method ((x t)) nil))


;;; area-dataset --

(defclass area-dataset (group-dataset) ())

(defgeneric area-dataset-p (x)
  (:method ((x area-dataset)) t)
  (:method ((x t)) nil))


;;; stack-dataset --

(defclass stack-dataset (group-dataset) ())

(defgeneric stack-dataset-p (x)
  (:method ((x stack-dataset)) t)
  (:method ((x t)) nil))


;;; Protocol
;;; --------

(defgeneric dataset-rank (d))

(defgeneric dataset-dimensions (d))

(defgeneric dataset-dimension (d axis))


#| Not yet
(defgeneric destructure-dataset (data))
|#

;;; make-dataset*
;;; make-dataset
;;;
;;; The two utility constructors.  The second is actually a function
;;; defined in 'dataset-implementation'.

(defgeneric make-dataset* (data &key name &allow-other-keys)
  (:documentation
   "Creates a dataset named NAME from DATA.")
  )

(declaim (ftype (function (&key
                           (:name (or string symbol))
                           (:x-data sequence)
                           (:y-data sequence)
                           (:z-data sequence))
                          dataset)
                make-dataset))


(defgeneric make-shaped-dataset (data shape dimensions
                                      &key
                                      &allow-other-keys)
  (:documentation
   "Creates a dataset from DATA given a SHAPE and a set of DIMENSIONS.")
  )


;;; PLot Component protocol.
;;; Maybe move it in the '-implementation' file.

(defmethod name-of ((d dataset)) (dataset-name d))

(defmethod part-of ((d dataset)) (in-graph d))

(defmethod (setf part-of) (g (d dataset))
  (setf (in-graph d) g))


(defmethod parent ((d dataset)) (in-graph d))

(defmethod (setf parent) (g (d dataset))
  (setf (in-graph d) g))


(defmethod represents ((d dataset)) (dataset-raw-data d))

(defmethod representation ((d dataset)) (dataset-raw-data d))

(defmethod components ((d dataset)) (list (dataset-raw-data d)))


;;;; end of file -- dataset.lisp --
