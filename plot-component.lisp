;;;; -*- Mode: Lisp -*-

;;;; plot-component.lisp
;;;;
;;;; Plotting utilities for Common Lisp; basic definitions.
;;;;
;;;; See the file COPYING for copyright and licensing information.
;;;;
;;;; Notes:
;;;; 
;;;; 20230202 MA: Too complicated to have mirroring.  Define a
;;;; protocol instead.

(in-package "CL-PLOT")


;;; plot-component --
;;;
;;; A PLOT-COMPONENT is anything that implements the following
;;; protocol.  I.e., naming and a part-of relation.
;;; As a matter of fact, this protocol will be implemented by actual
;;; implementation objects (they do anyway, AFAIK).

(defgeneric name-of (plot-component))


;;; PART-OF and PARENT are synonims.
;;; They must all be implemented.

(defgeneric part-of (plot-component)
  (:method ((pc t)) nil))


(defgeneric parent (plot-component)
  (:method ((pc t)) nil))


;;; REPRESENTS, and REPRESENTATION are synonims.
;;; They must all be implemented.

(defgeneric represents (plot-component)
  (:method ((pc t)) nil))


(defgeneric representation (plot-component)
  (:method ((pc t)) nil))


;;; COMPONENTS, i.e., what is inside.

(defgeneric components (plot-component)
  (:method ((pc t)) nil))


;;;; end of file -- plot-component.lisp --
