;;;; -*- Mode: Lisp -*-

;;;; math-utilities.lisp
;;;;
;;;; Plotting utilities for LW; various math utility functions.
;;;;
;;;; See the file COPYING for copyright and licensing information.

(in-package "CL-PLOT")

(eval-when (:compile-toplevel :load-toplevel :execute)
  ;; Necessary for the DEFCONSTANT below.

(defun magnitude (x)
  "Returns the magnitude of a real number.

The order of magnitude is the exponent of 10 in the floating point
representation of X.  The function returns two values: the magnitude
and the sign of X."

  (declare (type real x))
  (let ((s (signum x)))
    (if (zerop s)
        (values 0 s)
        (values (floor (log (abs x) 10)) s))))

) ; eval-when


(defconstant +dfem+ (magnitude double-float-epsilon))

(declaim (type fixnum +dfem+))


(defun choose-step (start end)
  "Chooses an appropriate 'step' size between START and END.

This is an internal function.  The result is a number that is an order
of magnitude 'lower' than that of 'END - START'.  The lowest number
returned is DOUBLE-FLOAT-EPSILON.

Exceptional Situations:

The assertion '(<= START END)' must hold.  Also, START and END cannot
be infinities.

Notes:

Infinities and NaNs are not properly handled."

  (declare (type real start end))

  (assert (<= most-negative-long-float
              start
              end
              most-positive-long-float)
      (start end))
  
  (if (= start end)
      double-float-epsilon
      (let* ((span (- end start))
             (ms (magnitude span))
             (ms-1 (1- ms))
             (span-type (type-of span))
             (step-type (if (subtypep span-type 'integer)
                            'integer
                            span-type))
             )
        (declare (type real span)
                 (type fixnum ms ms-1))

        (cond ((<= ms +dfem+)
               ;; Too small (it cannot be lower, but the test is still ok).
               (values double-float-epsilon +dfem+))
 
              ((minusp ms)
               (if (= ms (1+ +dfem+))
                   (values double-float-epsilon +dfem+)
                   ;; else (> ms (1+ +dfem+)
                   ;; Return a 1 of a magnitude less (with a guessed type).
                   (values (expt (coerce 10 step-type) ms-1) ms-1)))

              ((>= ms 0)
               (values (expt (coerce 10 step-type) ms-1) ms-1))

              (t
               ;; Should never get here...
               (error "Cannot guess the step size."))
              ))
      ))


(declaim (ftype (function (real real real) list) linspace-list))

(defun linspace-list (start end step)
  (declare (type real start end step))

  (assert (< start end) (start end))
  (assert (plusp step) (step))

  (loop with i of-type real = start
        unless (> i end)
        collect i
        until (>= i end)
        do (incf i step)))


(declaim (ftype (function (real
                           &optional
                           real
                           real
                           (or (eql list) (eql vector))
                           )
                          (or list vector))
                range))

(defun range (start
              &optional
              (end (1+ start))
              (step (choose-step start end))
              (result-type 'vector))

  (declare (type real start end step)
           (type fixnum intervals)
           (type (or (eql list) (eql vector)) result-type)
           (type boolean intervals-supplied-p step-supplied-p))
  
  (let ((result (linspace-list start end step)))
    (declare (type list result))
    (if (subtypep result-type 'list)
        result
        (coerce result result-type))
    ))


(declaim (ftype (function (real
                           &optional
                           real
                           &key
                           (:result-type (or (eql list) (eql vector)))
                           (:intervals fixnum)
                           )
                          (or list vector))
                linspace))

(defun linspace (start
                 &optional
                 (end (1+ start))
                 &key
                 (result-type 'vector)
                 (intervals 10)
                 )
  "Returns a sequence of equally spaced values."

  ;; Mixing &optional and &key is bad form, but it looks nice here.

  (declare (type real start end)
           (type fixnum intervals)
           (type (or (eql list) (eql vector)) result-type)
           )

  (range start end (/ (- end start) intervals) result-type)

  #|
  (let ((result (linspace-list start end (/ (- end start) intervals))))
    (declare (type list result))
    (if (subtypep result-type 'list)
        result
        (coerce result result-type)))
  |#
  )


(declaim (ftype (function (&key
                           (:start real)
                           (:end real)
                           (:step real)
                           (:result-type (or (eql list) (eql vector)))
                           (:intervals fixnum)
                           )
                          (or list vector))
                intervals))

(defun intervals (&key
                  (start 0)
                  (end (1+ start))
                  (intervals 10 intervals-supplied-p)
                  (step 1 step-supplied-p)
                  (result-type 'vector))

  (declare (type real start end step)
           (type fixnum intervals)
           (type boolean intervals-supplied-p step-supplied-p))

  (cond ((and step-supplied-p intervals-supplied-p)
         (error "STEP and INTERVALS cannot be both supplied."))

        (intervals-supplied-p
         ;; STEP was not supplied
         (setq step (/ (- end start) intervals)))
        
        (step-supplied-p #| Do nothing |#)

        (t
         (setq step (choose-step start end)))
        )
  
  (range start end step result-type))
  
              
;;;; end of file -- math-utilities.lisp
